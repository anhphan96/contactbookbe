package com.anh.contactbook.jwt;

import com.anh.contactbook.exception.AuthenticationFailException;
import com.anh.contactbook.model.CustomUserDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class JwtAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

    @Autowired
    private JwtTokenValidator jwtTokenValidator;

    @Override
    public boolean supports(Class<?> authentication) {
        return (JwtAuthenticationToken.class.isAssignableFrom(authentication));
    }

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken) throws AuthenticationException {
    }

    @Override
    protected UserDetails retrieveUser(String s, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        JwtAuthenticationToken jwtAuthenticationToken = (JwtAuthenticationToken) authentication;
        String token = jwtAuthenticationToken.getToken();

        JwtTokenDTO parsedUser = jwtTokenValidator.parseToken(token);

        if (parsedUser == null) {
            throw new AuthenticationFailException("Authenticated fail!");
        }

        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();

        parsedUser.getFunctionRoles().stream().forEach(r ->
            grantedAuthorities.add(new SimpleGrantedAuthority(r))
        );

        return new CustomUserDetail(parsedUser.getUsername(), token, parsedUser.getEmail(), grantedAuthorities);
    }
}

package com.anh.contactbook.jwt;

import java.util.List;

public class JwtTokenDTO {

    private String username;

    private String email;

    private List<String> functionRoles;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<String> getFunctionRoles() {
        return functionRoles;
    }

    public void setFunctionRoles(List<String> functionRoles) {
        this.functionRoles = functionRoles;
    }

}

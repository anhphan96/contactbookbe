package com.anh.contactbook.jwt;

import com.anh.contactbook.exception.AuthenticationFailException;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.keys.RsaKeyUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.PublicKey;
import java.util.*;

@Component
public class JwtTokenValidator {

    private final ResourceLoader resourceLoader;

    @Autowired
    public JwtTokenValidator(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    public JwtTokenDTO parseToken(String token) {
        JwtTokenDTO jwtUser;

        try {
            Resource resource = resourceLoader.getResource("classpath:static/publickey.txt");
            String publicKeyString = readFromInputStream(resource.getInputStream());

            RsaKeyUtil rsaKeyUtil = new RsaKeyUtil();
            PublicKey publicKey = rsaKeyUtil.fromPemEncoded(publicKeyString);

            JwtConsumer jwtConsumer = new JwtConsumerBuilder()
                    .setRequireExpirationTime()
                    .setVerificationKey(publicKey)
                    .setSkipDefaultAudienceValidation()
                    .build();

            JwtClaims jwtDecoded = jwtConsumer.processToClaims(token);

            String username = jwtDecoded.getStringClaimValue("preferred_username");
            String email = jwtDecoded.getStringClaimValue("email");

            Map<String, Object> resource_access = jwtDecoded.getClaimValue("resource_access", Map.class);

            Object myClient = resource_access.get("360-feedback");
            jwtUser = new JwtTokenDTO();
            jwtUser.setUsername(username);
            jwtUser.setEmail(email);
            jwtUser.setFunctionRoles(new ArrayList<>());
            if (Objects.nonNull(myClient)){
                HashMap<String, List<String>> clientMap = (HashMap<String, List<String>>) myClient;
                List<String> functionRoles = clientMap.get("roles");
                jwtUser.setFunctionRoles(functionRoles);
            }
        } catch (Exception e) {
            throw new AuthenticationFailException("Authenticated fail!");
        }
        return jwtUser;
    }

    private String readFromInputStream(InputStream inputStream) {
        StringBuilder resultStringBuilder = new StringBuilder();
        try (BufferedReader br
                     = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = br.readLine()) != null) {
                resultStringBuilder.append(line).append("\n");
            }
        } catch (IOException e) {
            return "";
        }
        return resultStringBuilder.toString();
    }
}

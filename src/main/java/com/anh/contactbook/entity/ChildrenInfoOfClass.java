package com.anh.contactbook.entity;

import org.springframework.data.annotation.Transient;

import java.util.Date;

public class ChildrenInfoOfClass {
    private String childId;
    @Transient
    private String childName;
    @Transient
    private Date birthday;

    public ChildrenInfoOfClass() {
    }

    public ChildrenInfoOfClass(String childId, String childName, Date birthday) {
        this.childId = childId;
        this.childName = childName;
        this.birthday = birthday;
    }

    public String getChildId() {
        return childId;
    }

    public void setChildId(String childId) {
        this.childId = childId;
    }

    public String getChildName() {
        return childName;
    }

    public void setChildName(String childName) {
        this.childName = childName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }
}

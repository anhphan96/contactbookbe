package com.anh.contactbook.entity;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

@Document
public class Class {

    @Id
    private String id;

    private String name;

    private Boolean active;

    private List<TeacherInfoOfClass> teacherInfoOfClasses;

    private List<ChildrenInfoOfClass> childrenInfoOfClasses;

    private Date endedAt;

    @TextIndexed
    @CreatedDate
    private Date createdAt;

    @LastModifiedDate
    private Date updatedAt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public List<TeacherInfoOfClass> getTeacherInfoOfClasses() {
        return teacherInfoOfClasses;
    }

    public void setTeacherInfoOfClasses(List<TeacherInfoOfClass> teacherInfoOfClasses) {
        this.teacherInfoOfClasses = teacherInfoOfClasses;
    }

    public List<ChildrenInfoOfClass> getChildrenInfoOfClasses() {
        return childrenInfoOfClasses;
    }

    public void setChildrenInfoOfClasses(List<ChildrenInfoOfClass> childrenInfoOfClasses) {
        this.childrenInfoOfClasses = childrenInfoOfClasses;
    }

    public Date getEndedAt() {
        return endedAt;
    }

    public void setEndedAt(Date endedAt) {
        this.endedAt = endedAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}

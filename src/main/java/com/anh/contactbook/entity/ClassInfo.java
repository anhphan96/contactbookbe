package com.anh.contactbook.entity;

import org.springframework.data.annotation.Transient;

import java.util.Date;

public class ClassInfo {
    private String classId;
    private Date from;
    private Date to;
    @Transient
    private String className;

    public ClassInfo() {
    }

    public ClassInfo(String classId, Date from, Date to, String className) {
        this.classId = classId;
        this.from = from;
        this.to = to;
        this.className = className;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }
}

package com.anh.contactbook.entity;

import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
public class SystemLog {
    private String id;
    private Date createdAt;
    private String content;
    private String icon;
    private String childId;
    private boolean seen;
    private String classId;
    @Transient
    private Object dynamicObject;

    public SystemLog(){}

    public SystemLog(Date createdAt, String content, String icon, String childId, boolean seen) {
        this.createdAt = createdAt;
        this.content = content;
        this.icon = icon;
        this.childId = childId;
        this.seen = seen;
    }

    public SystemLog(SystemLog srcSys) {
        this.id = srcSys.getId();
        this.createdAt = srcSys.getCreatedAt();
        this.content = srcSys.getContent();
        this.icon = srcSys.getIcon();
        this.childId = srcSys.getChildId();
        this.seen = srcSys.getSeen();
        this.classId = srcSys.getClassId();
        this.dynamicObject = srcSys.getDynamicObject();
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getChildId() {
        return childId;
    }

    public void setChildId(String childId) {
        this.childId = childId;
    }

    public boolean getSeen() {
        return seen;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }

    public boolean isSeen() {
        return seen;
    }

    public Object getDynamicObject() {
        return dynamicObject;
    }

    public void setDynamicObject(Object dynamicObject) {
        this.dynamicObject = dynamicObject;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }
}

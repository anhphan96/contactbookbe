package com.anh.contactbook.entity;

import org.springframework.data.annotation.LastModifiedDate;

import java.util.Date;

public class Schedule {
    private String morning;

    private String afternoon;

    @LastModifiedDate
    private Date updatedAt;

    public String getMorning() {
        return morning;
    }

    public void setMorning(String morning) {
        this.morning = morning;
    }

    public String getAfternoon() {
        return afternoon;
    }

    public void setAfternoon(String afternoon) {
        this.afternoon = afternoon;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}

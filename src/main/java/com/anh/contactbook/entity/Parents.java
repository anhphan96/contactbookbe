package com.anh.contactbook.entity;

import com.anh.contactbook.enums.Gender;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

@Document
public class Parents {

    @Id
    private String id;

    private String name;

    @TextIndexed
    private Date birthDay;

    private Gender gender;

    private String phone;

    private String userName;

    private String password;

    private String kc_Id;

    private List<ChildrenInfoOfClass> childrenInfoOfClasses;

    @CreatedDate
    private Date createdAt;

    @LastModifiedDate
    private Date updatedAt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(Date birthDay) {
        this.birthDay = birthDay;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getKc_Id() {
        return kc_Id;
    }

    public void setKc_Id(String kc_Id) {
        this.kc_Id = kc_Id;
    }

    public List<ChildrenInfoOfClass> getChildrenInfoOfClasses() {
        return childrenInfoOfClasses;
    }

    public void setChildrenInfoOfClasses(List<ChildrenInfoOfClass> childrenInfoOfClasses) {
        this.childrenInfoOfClasses = childrenInfoOfClasses;
    }
}

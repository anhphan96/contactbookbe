package com.anh.contactbook.entity;

public class Hygience {
    private int numberOfWc;

    private String note;

    public int getNumberOfWc() {
        return numberOfWc;
    }

    public void setNumberOfWc(int numberOfWc) {
        this.numberOfWc = numberOfWc;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}

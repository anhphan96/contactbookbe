package com.anh.contactbook.entity;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
public class Heal {
    @Id
    private String id;
    private String childId;
    private String name;
    private double weight = 0;
    private double heigh = 0;
    private int age = 0;
    private double highStandardOfHeight;
    private double highStandardOfWeight;
    private double lowStandardOfHeight;
    private double lowStandardOfWeight;
    private String gender;

    @CreatedDate
    private Date createdAt;

    @LastModifiedDate
    private Date updatedAt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getChildId() {
        return childId;
    }

    public void setChildId(String childId) {
        this.childId = childId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getHeigh() {
        return heigh;
    }

    public void setHeigh(double heigh) {
        this.heigh = heigh;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public double getHighStandardOfHeight() {
        return highStandardOfHeight;
    }

    public void setHighStandardOfHeight(double highStandardOfHeight) {
        this.highStandardOfHeight = highStandardOfHeight;
    }

    public double getHighStandardOfWeight() {
        return highStandardOfWeight;
    }

    public void setHighStandardOfWeight(double highStandardOfWeight) {
        this.highStandardOfWeight = highStandardOfWeight;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public double getLowStandardOfHeight() {
        return lowStandardOfHeight;
    }

    public void setLowStandardOfHeight(double lowStandardOfHeight) {
        this.lowStandardOfHeight = lowStandardOfHeight;
    }

    public double getLowStandardOfWeight() {
        return lowStandardOfWeight;
    }

    public void setLowStandardOfWeight(double lowStandardOfWeight) {
        this.lowStandardOfWeight = lowStandardOfWeight;
    }
}

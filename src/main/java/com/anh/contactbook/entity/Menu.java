package com.anh.contactbook.entity;

import java.util.Date;
import java.util.List;

public class Menu {
    private String morning;

    private String lunch;

    private String afternoon;

    private Date updatedAt;

    private List<ParentsComment> parentsComments;

    private List<TeacherComment> teacherComments;

    public String getMorning() {
        return morning;
    }

    public void setMorning(String morning) {
        this.morning = morning;
    }

    public String getLunch() {
        return lunch;
    }

    public void setLunch(String lunch) {
        this.lunch = lunch;
    }

    public String getAfternoon() {
        return afternoon;
    }

    public void setAfternoon(String afternoon) {
        this.afternoon = afternoon;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<ParentsComment> getParentsComments() {
        return parentsComments;
    }

    public void setParentsComments(List<ParentsComment> parentsComments) {
        this.parentsComments = parentsComments;
    }

    public List<TeacherComment> getTeacherComments() {
        return teacherComments;
    }

    public void setTeacherComments(List<TeacherComment> teacherComments) {
        this.teacherComments = teacherComments;
    }
}

package com.anh.contactbook.entity;

import org.springframework.data.annotation.Transient;

public class TeacherInfoOfClass {
    private String teacherId;
    @Transient
    private String teacherName;

    public TeacherInfoOfClass() {
    }

    public TeacherInfoOfClass(String teacherId, String teacherName) {
        this.teacherId = teacherId;
        this.teacherName = teacherName;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }
}

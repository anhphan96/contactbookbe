package com.anh.contactbook.entity;

import java.util.Date;

public class ChildrenAttendance {
    private String childId;

    private String name;

    private Date birthDay;

    private String note;

    private Boolean absent = false;

    private Boolean permission = false;

    private String pickupTime;

    private String pickupPerson;

    public String getChildId() {
        return childId;
    }

    public void setChildId(String childId) {
        this.childId = childId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Boolean getAbsent() {
        return absent;
    }

    public void setAbsent(Boolean absent) {
        this.absent = absent;
    }

    public Boolean getPermission() {
        return permission;
    }

    public void setPermission(Boolean permission) {
        this.permission = permission;
    }

    public String getPickupTime() {
        return pickupTime;
    }

    public void setPickupTime(String pickupTime) {
        this.pickupTime = pickupTime;
    }

    public String getPickupPerson() {
        return pickupPerson;
    }

    public void setPickupPerson(String pickupPerson) {
        this.pickupPerson = pickupPerson;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(Date birthDay) {
        this.birthDay = birthDay;
    }
}

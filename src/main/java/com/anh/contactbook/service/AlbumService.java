package com.anh.contactbook.service;

import com.anh.contactbook.entity.Album;

import java.util.List;

public interface AlbumService {
    Album createAlbum(Album album);
    Album getAlbumByAlbumId(String albumId);
    Album createPhotos(Album album);
    List<Album> getAlbumsByClassId(String classId);

    Album updateAlbum(Album album);

    void deleteAlbum(String albumId);
}

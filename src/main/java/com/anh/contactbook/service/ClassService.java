package com.anh.contactbook.service;

import com.anh.contactbook.entity.Class;

import java.util.List;

public interface ClassService {
    Class getClassByClassId(String classId);
    List<Class> getClasses();
    Class saveClass(Class cl);

    void closeClass(String classId);
}

package com.anh.contactbook.service;

import com.anh.contactbook.entity.Prescription;

import java.util.List;

public interface PrescriptionService {
    List<Prescription> getPrescriptionsByClassId(String classId);

    Prescription savePrescription(Prescription prescription);

    void confirmPrescription(String prescriptionId);

    List<Prescription> getIndividualPresciptions(String childId);

    Prescription createIndividualPresciptions(Prescription prescription);

    int countNotification(String childId);

    int countTeacherNotification(String classId);
}

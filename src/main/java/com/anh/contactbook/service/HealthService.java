package com.anh.contactbook.service;

import com.anh.contactbook.entity.Heal;
import com.anh.contactbook.model.ClassChildrenHealth;

import java.util.List;

public interface HealthService {
    List<Heal> getHealthByChildId(String childId);

    Heal saveChildHealth(String childId, Heal heal);

    List<ClassChildrenHealth> getHealthByClassId(String classId);

    void deleteHealthRecord(String healthId);
}

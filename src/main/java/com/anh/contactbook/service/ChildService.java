package com.anh.contactbook.service;

import com.anh.contactbook.entity.Child;

import java.util.List;

public interface ChildService {
    List<Child> getChildrenByClassId(String classId);
    Child getChildByChildId(String childId);
    List<Child> getChildren();
    Child saveChild(Child child);
    List<Child> getChildrenByParents(String parentsUserName);
}

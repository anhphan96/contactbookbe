package com.anh.contactbook.service;

import com.anh.contactbook.entity.Teacher;

import java.util.List;

public interface TeacherService {
    Teacher getTeacherByTeacherId(String teacherId);
    List<Teacher> getTeachers();
    Teacher saveAccount(Teacher teacher);
    Teacher saveTeacher(Teacher teacher);
    Teacher getTeacherByAccount(String username);
}

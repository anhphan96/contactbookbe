package com.anh.contactbook.service;

import com.anh.contactbook.entity.SystemLog;

import java.util.List;

public interface SystemLogService {
    void saveSystemLog(SystemLog systemLog);
    List<SystemLog> getSystemLogByChildId(String childId);
    int countNotification(String childId);
}

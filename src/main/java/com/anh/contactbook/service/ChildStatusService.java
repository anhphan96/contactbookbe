package com.anh.contactbook.service;

import com.anh.contactbook.entity.ChildStatus;

import java.util.List;

public interface ChildStatusService {
    List<ChildStatus> getChildStatusByCurrentDate(String classId, String createdAt);

    void save(List<ChildStatus> childStatuses);

    List<ChildStatus> getIndividualChildStatus(String childId, String date);
}

package com.anh.contactbook.service;

import com.anh.contactbook.entity.ClassStatus;
import com.anh.contactbook.entity.Menu;
import com.anh.contactbook.entity.Schedule;

import java.util.Date;
import java.util.List;

public interface ClassStatusService {
    List<ClassStatus> findClassStatusByClassIdAndCreatedAtBetween(String classId, Date createdAt);
    ClassStatus findClassStatusByClassIdAndCreatedAt(String classId, Date createdAt);
    void saveOrCreateClassStatus(ClassStatus classStatus);
    ClassStatus createMenu(Menu menu, String classId, Date createAt);
    ClassStatus updateMenu(ClassStatus classStatus, Date createAtOfMenu);
    List<ClassStatus> findClassStatusByClassIdInTheWeek(String classId, Date createAt);
    ClassStatus createSchedule(Schedule schedule, String classId, Date createAt);
    ClassStatus updateSchedule(ClassStatus classStatus, Date createAtOfSchedule);
}

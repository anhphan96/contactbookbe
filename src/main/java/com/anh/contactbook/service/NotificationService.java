package com.anh.contactbook.service;

import com.anh.contactbook.entity.Notification;

import java.util.List;

public interface NotificationService {
    List<Notification> getNotificationsByClassIdAndTeacherId(String classId, String teacherId);
    Notification saveNotification(Notification notification);
    List<Notification> getNotificationsByClassId(String classId);
}

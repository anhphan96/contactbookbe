package com.anh.contactbook.service;

import com.anh.contactbook.model.NotificationQuantity;

public interface NotificationQuantityService {
    NotificationQuantity getNotificationQuantityOfParent(String classId, String childId);
    NotificationQuantity getNotificationQuantityOfTeacher(String classId);
}

package com.anh.contactbook.service;

import com.anh.contactbook.entity.Attendance;
import com.anh.contactbook.model.IndividualAttendance;

import java.util.Date;
import java.util.List;

public interface AttendanceService {
    Attendance getAttendanceByCreatedDay(String classId, Date createdDay);
    void saveOrCreateAttendance(Attendance attendance);
    List<IndividualAttendance> getIndividualAttendance(String classId, String childrenId, Date currentDate);
}

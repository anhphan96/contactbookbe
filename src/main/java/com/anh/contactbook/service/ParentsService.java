package com.anh.contactbook.service;

import com.anh.contactbook.entity.Parents;
import javafx.scene.Parent;

import java.util.List;

public interface ParentsService {
    Parents getParentsByUserName(String username);
    Parents saveParents(Parents parents);
    Parents saveAccount(Parents parents);
    List<Parents> getParents();
    Parents getParentsById(String parentsId);
}

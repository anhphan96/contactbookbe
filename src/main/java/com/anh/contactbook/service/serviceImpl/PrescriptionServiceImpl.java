package com.anh.contactbook.service.serviceImpl;

import com.anh.contactbook.controller.WebSockerController;
import com.anh.contactbook.entity.*;
import com.anh.contactbook.entity.Class;
import com.anh.contactbook.exception.EntityNotFoundException;
import com.anh.contactbook.exception.InvalidParamException;
import com.anh.contactbook.repository.PrescriptionRepository;
import com.anh.contactbook.service.ChildService;
import com.anh.contactbook.service.ClassService;
import com.anh.contactbook.service.PrescriptionService;
import com.anh.contactbook.service.SystemLogService;
import com.anh.contactbook.untility.DateUtil;
import org.joda.time.DateTimeComparator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class PrescriptionServiceImpl implements PrescriptionService {

    private final PrescriptionRepository prescriptionRepository;

    private final ClassService classService;

    private final ChildService childService;

    private final SystemLogService systemLogService;

    private final WebSockerController webSockerController;

    @Autowired
    public PrescriptionServiceImpl(PrescriptionRepository prescriptionRepository, ClassService classService, ChildService childService, SystemLogService systemLogService, WebSockerController webSockerController) {
        this.prescriptionRepository = prescriptionRepository;
        this.classService = classService;
        this.childService = childService;
        this.systemLogService = systemLogService;
        this.webSockerController = webSockerController;
    }

    @Override
    public List<Prescription> getPrescriptionsByClassId(String classId) {
        Class currentClass = classService.getClassByClassId(classId);
//        List<Prescription> prescriptions = new ArrayList<>();
//        currentClass.getChildrenId().forEach(c -> {
//            List<Prescription> pres = prescriptionRepository.getPrescriptionByChildId(c);
//            prescriptions.addAll(pres);
//        });
        return prescriptionRepository.getPrescriptionByClassIdOrderByConfirmAscCompleteAscCreatedAtDesc(classId);
    }

    @Override
    public Prescription savePrescription(Prescription prescription) {
        if (Objects.isNull(prescription.getChildId())) {
            throw new InvalidParamException("Save prescription fail!");
        }
        if (prescription.getProgresses().stream().allMatch(Progress::getConfirm)) {
            prescription.setComplete(true);
        }else {
            prescription.setComplete(false);
        }
        prescription.setUpdatedAt(DateUtil.getDataBaseDate(DateUtil.plusDate(new Date(), 1)));
        prescriptionRepository.save(prescription);

        SystemLog systemLog = new SystemLog(new Date(),
                "Teacher updated progress of prescription " + prescription.getPathological() + ", let check it out!",
                "assignment",
                prescription.getChildId(),
                false);
        systemLog.setDynamicObject(prescription);
        systemLogService.saveSystemLog(systemLog);
        webSockerController.confirmApplicationLog("prescription", prescription.getChildId(), systemLog);
        return prescription;
    }

    @Override
    public void confirmPrescription(String prescriptionId) {
        Prescription prescription = prescriptionRepository.findById(prescriptionId)
                .orElseThrow(() -> new EntityNotFoundException("No prescription found!"));
        prescription.setConfirm(true);
        prescription.setUpdatedAt(DateUtil.getDataBaseDate(DateUtil.plusDate(new Date(), 1)));
        prescriptionRepository.save(prescription);

        SystemLog systemLog = new SystemLog(new Date(),
                "Your prescription" + prescription.getPathological() + " is confirmed by Teacher!",
                "assignment",
                prescription.getChildId(),
                false);
        systemLog.setDynamicObject(prescriptionId);
        systemLogService.saveSystemLog(systemLog);
        webSockerController.confirmApplicationLog("prescription", prescription.getChildId(), systemLog);
    }

    @Override
    public List<Prescription> getIndividualPresciptions(String childId) {
        return prescriptionRepository.getPrescriptionByChildIdOrderByCreatedAtDesc(childId);
    }

    @Override
    public Prescription createIndividualPresciptions(Prescription prescription) {
        if (DateTimeComparator.getDateOnlyInstance().compare(prescription.getFromDay(), prescription.getToDay()) == 1 || prescription.getChildId() == null || prescription.getPathological() == null || prescription.getMedicines() == null) {
            throw new InvalidParamException("Information is invalid!");
        }

        Child child = childService.getChildByChildId(prescription.getChildId());
        prescription.setConfirm(false);
        prescription.setCreatedAt(DateUtil.getDataBaseDate(DateUtil.plusDate(new Date(), 1)));
        prescription.setUpdatedAt(DateUtil.getDataBaseDate(DateUtil.plusDate(new Date(), 1)));
        prescription.setClassId(child.getCurrentClassId());
        prescription.setComplete(false);
        prescription.setChildName(child.getName());
        prescription.setFromDay(DateUtil.plusDate(DateUtil.getDataBaseDate(prescription.getFromDay()), 1));
        prescription.setToDay(DateUtil.plusDate(DateUtil.getDataBaseDate(prescription.getToDay()), 1));
        prescription.setProgresses(getProcess(prescription.getFromDay(), prescription.getToDay()));
        prescriptionRepository.save(prescription);

        SystemLog systemLog = new SystemLog(new Date(),
                child.getName() + " has a new prescription!",
                "assignment",
                null,
                false);
        systemLog.setDynamicObject(prescription);
        webSockerController.confirmApplicationLog("prescription", child.getCurrentClassId(), systemLog);
        return prescription;
    }

    @Override
    public int countNotification(String childId) {
        return prescriptionRepository.countPrescriptionByChildIdAndCompleteFalse(childId);
    }

    @Override
    public int countTeacherNotification(String classId) {
        return prescriptionRepository.countPrescriptionByClassIdAndCompleteFalse(classId);
    }

    private List<Progress> getProcess(Date fromDay, Date toDay) {
        List<Progress> progresses = new ArrayList<>();
        if (DateTimeComparator.getDateOnlyInstance().compare(fromDay, toDay) == 0) {
            Progress progress = new Progress();
            progress.setConfirm(false);
            progress.setProcessDate(fromDay);
            progresses.add(progress);
        } else {
            Date from = fromDay;
            while (DateTimeComparator.getDateOnlyInstance().compare(from, toDay) != 1) {
                Progress progress = new Progress();
                progress.setConfirm(false);
                progress.setProcessDate(from);
                progresses.add(progress);
                from = DateUtil.plusDate(from, 1);
            }
        }
        return progresses;
    }
}

package com.anh.contactbook.service.serviceImpl;

import com.anh.contactbook.controller.WebSockerController;
import com.anh.contactbook.entity.Child;
import com.anh.contactbook.entity.Class;
import com.anh.contactbook.entity.Heal;
import com.anh.contactbook.entity.SystemLog;
import com.anh.contactbook.exception.EntityNotFoundException;
import com.anh.contactbook.exception.InvalidParamException;
import com.anh.contactbook.model.ClassChildrenHealth;
import com.anh.contactbook.repository.HealthRepository;
import com.anh.contactbook.service.ChildService;
import com.anh.contactbook.service.ClassService;
import com.anh.contactbook.service.HealthService;
import com.anh.contactbook.service.SystemLogService;
import com.anh.contactbook.untility.DateUtil;
import com.anh.contactbook.untility.ZScoreUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class HealthServiceImpl implements HealthService {

    private final HealthRepository healthRepository;

    private final ChildService childService;

    private final ClassService classService;

    private final SystemLogService systemLogService;

    private final WebSockerController webSockerController;

    @Autowired
    public HealthServiceImpl(HealthRepository healthRepository, ChildService childService, ClassService classService, SystemLogService systemLogService, WebSockerController webSockerController) {
        this.healthRepository = healthRepository;
        this.childService = childService;
        this.classService = classService;
        this.systemLogService = systemLogService;
        this.webSockerController = webSockerController;
    }

    @Override
    public List<Heal> getHealthByChildId(String childId) {
        return healthRepository.getHealByChildIdOrderByAgeDesc(childId);
    }

    @Override
    public Heal saveChildHealth(String childId, Heal heal) {
        SystemLog systemLog = null;
        Child child = childService.getChildByChildId(childId);
        if (heal.getId() == null && healthRepository.getHealByAge(child.getAge()).size() > 0) {
            throw new InvalidParamException("This child already has the record in this month!");
        }
        heal.setChildId(childId);
        heal.setName(child.getName());
        heal.setGender(child.getGender().toString());
        if (StringUtils.isEmpty(heal.getId())) {
            heal.setAge(child.getAge());
        }
        if (heal.getHeigh() > 0) {
            heal.setHighStandardOfHeight(ZScoreUtil.getStandard(child.getGender().toString(), heal.getAge(), "height", "high"));
            heal.setLowStandardOfHeight(ZScoreUtil.getStandard(child.getGender().toString(), heal.getAge(), "height", "low"));
        }
        if (heal.getWeight() > 0) {
            heal.setHighStandardOfWeight(ZScoreUtil.getStandard(child.getGender().toString(), heal.getAge(), "weight", "high"));
            heal.setLowStandardOfWeight(ZScoreUtil.getStandard(child.getGender().toString(), heal.getAge(), "weight", "low"));
        }
        if (StringUtils.isEmpty(heal.getId()) || Objects.isNull(heal.getId())) {
            heal.setCreatedAt(DateUtil.getDataBaseDate(DateUtil.plusDate(new Date(), 1)));
            systemLog = new SystemLog(new Date(),
                    "Teacher have added new health record!",
                    "add_box",
                    childId,
                    false);
            systemLog.setDynamicObject(heal);
        }
        heal.setUpdatedAt(DateUtil.getDataBaseDate(DateUtil.plusDate(new Date(), 1)));
        healthRepository.save(heal);
        if (Objects.nonNull(systemLog)) {
            systemLogService.saveSystemLog(systemLog);
            webSockerController.confirmApplicationLog("health", childId, systemLog);
        }
        return heal;
    }

    @Override
    public List<ClassChildrenHealth> getHealthByClassId(String classId) {
        Class currentClass = classService.getClassByClassId(classId);
        List<ClassChildrenHealth> classChildrenHealths = new ArrayList<>();
        currentClass.getChildrenInfoOfClasses().forEach(c -> {
            Child child = childService.getChildByChildId(c.getChildId());
            ClassChildrenHealth classChildrenHealth = new ClassChildrenHealth();
            classChildrenHealth.setChildId(child.getId());
            classChildrenHealth.setBirthDate(child.getBirthDay());
            classChildrenHealth.setChildName(child.getName());
            classChildrenHealth.setChildHealth(healthRepository.getHealByChildIdOrderByAgeDesc(c.getChildId()));
            classChildrenHealth.setAge(child.getAge());
            classChildrenHealths.add(classChildrenHealth);
        });
        return classChildrenHealths;
    }

    @Override
    public void deleteHealthRecord(String healthId) {
        Heal heal = healthRepository.findById(healthId).orElseThrow(() -> new EntityNotFoundException("no health found!"));
        this.healthRepository.delete(heal);
    }
}

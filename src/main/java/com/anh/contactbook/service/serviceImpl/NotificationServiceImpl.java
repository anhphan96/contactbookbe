package com.anh.contactbook.service.serviceImpl;

import com.anh.contactbook.controller.WebSockerController;
import com.anh.contactbook.entity.*;
import com.anh.contactbook.entity.Class;
import com.anh.contactbook.exception.InvalidParamException;
import com.anh.contactbook.repository.NotificationRepository;
import com.anh.contactbook.service.*;
import com.anh.contactbook.untility.DateUtil;
import com.anh.contactbook.untility.PhotoUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class NotificationServiceImpl implements NotificationService {
    private final NotificationRepository notificationRepository;
    private final TeacherService teacherService;
    private final ClassService classService;
    private final ChildService childService;

    private final SystemLogService systemLogService;

    private final WebSockerController webSockerController;

    @Autowired
    public NotificationServiceImpl(NotificationRepository notificationRepository, TeacherService teacherService, ClassService classService, WebSockerController webSockerController, SystemLogService systemLogService, ChildService childService) {
        this.notificationRepository = notificationRepository;
        this.teacherService = teacherService;
        this.classService = classService;
        this.webSockerController = webSockerController;
        this.systemLogService = systemLogService;
        this.childService = childService;
    }


    @Override
    public List<Notification> getNotificationsByClassIdAndTeacherId(String classId, String teacherId) {
        return notificationRepository.findNotificationByClassIdAndTeacherIdOrderByCreatedAtDesc(classId, teacherId);
    }

    @Override
    public Notification saveNotification(Notification notification) {
        if (Objects.isNull(notification.getTitle()) || notification.getTitle().equals("")) {
            throw new InvalidParamException("No title!");
        }
        if (Objects.isNull(notification.getContent()) || notification.getContent().equals("")) {
            notification.setContent(notification.getTitle());
        }
        Teacher teacher = teacherService.getTeacherByTeacherId(notification.getTeacherId());
        Class currentClsas = classService.getClassByClassId(notification.getClassId());
        notification.setTeacherName(teacher.getName());
        if (Objects.nonNull(notification.getImage())) {
            Image newImage = new Image();
            try {
                newImage.setUrl(PhotoUtil.saveImage(notification.getImage().getBase64().split(",")[1]));
            } catch (Exception e) {
                throw new InvalidParamException("Image is invalid!");
            }
            notification.setImage(newImage);
        }
        notification.setCreatedAt(new Date());
        notification.setUpdatedAt(new Date());
        notificationRepository.save(notification);
        List<Child> children = childService.getChildrenByClassId(currentClsas.getId());
        children.forEach(c -> {
            SystemLog systemLog = new SystemLog(new Date(),
                    "Teacher " + teacher.getName() + " has a new notification",
                    "notification_important", c.getId(),
                    false);
            systemLog.setDynamicObject(notification);
            systemLogService.saveSystemLog(systemLog);
            webSockerController.confirmApplicationLog("noti", c.getId(), systemLog);
        });
        return notification;
    }

    @Override
    public List<Notification> getNotificationsByClassId(String classId) {
        return notificationRepository.findNotificationByClassIdOrderByCreatedAtDesc(classId);
    }
}

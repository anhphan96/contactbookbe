package com.anh.contactbook.service.serviceImpl;

import com.anh.contactbook.entity.SystemLog;
import com.anh.contactbook.repository.SystemLogRepository;
import com.anh.contactbook.service.SystemLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SystemLogServiceImpl implements SystemLogService {

    private final SystemLogRepository systemLogRepository;

    @Autowired
    public SystemLogServiceImpl(SystemLogRepository systemLogRepository) {
        this.systemLogRepository = systemLogRepository;
    }

    @Override
    public void saveSystemLog(SystemLog systemLog) {
        systemLogRepository.save(systemLog);
    }

    @Override
    public List<SystemLog> getSystemLogByChildId(String childId) {
        List<SystemLog> systemLogs = systemLogRepository.findSystemLogByChildIdOrderByCreatedAtDesc(childId);
        List<SystemLog> returnSystemLog = new ArrayList<>();
        copyListSystemLogs(systemLogs, returnSystemLog);
        systemLogs.stream().filter(l -> !l.getSeen())
                .collect(Collectors.toList()).forEach(s -> {
                    s.setSeen(true);
                    systemLogRepository.save(s);
        });
        return returnSystemLog;
    }

    @Override
    public int countNotification(String childId) {
        return systemLogRepository.countSystemLogByChildIdAndSeenFalse(childId);
    }

    private void copyListSystemLogs(List<SystemLog> src, List<SystemLog> des) {
        src.forEach(s -> des.add(new SystemLog(s)));
    }
}

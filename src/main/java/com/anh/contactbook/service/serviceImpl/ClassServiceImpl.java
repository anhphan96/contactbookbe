package com.anh.contactbook.service.serviceImpl;

import com.anh.contactbook.entity.Child;
import com.anh.contactbook.entity.Class;
import com.anh.contactbook.entity.Teacher;
import com.anh.contactbook.exception.EntityNotFoundException;
import com.anh.contactbook.repository.ClassRepository;
import com.anh.contactbook.service.ChildService;
import com.anh.contactbook.service.ClassService;
import com.anh.contactbook.service.TeacherService;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.Date;
import java.util.List;

import static java.util.Arrays.asList;

@Service
public class ClassServiceImpl implements ClassService {

    private final ClassRepository classRepository;

    private final ChildService childService;

    private final TeacherService teacherService;

    @Autowired
    public ClassServiceImpl(ClassRepository classRepository,
                            @Lazy ChildService childService,
                            @Lazy TeacherService teacherService) {
        this.classRepository = classRepository;
        this.childService = childService;
        this.teacherService = teacherService;
    }

    @Override
    public Class getClassByClassId(String classId) {
        if (classRepository.findClassById(classId).isPresent()) {
            return classRepository.findClassById(classId).get();
        }
        throw new EntityNotFoundException("No class found!");
    }

    @Override
    public List<Class> getClasses() {
        List<Class> classes = classRepository.findClassByOrderByCreatedAtDesc();
        classes.forEach(cl -> {
            if (cl.getChildrenInfoOfClasses() != null) {
                cl.getChildrenInfoOfClasses().forEach(ch -> ch.setChildName(getChildName(ch.getChildId())));
            }
            if (cl.getTeacherInfoOfClasses() != null) {
                cl.getTeacherInfoOfClasses().forEach(t -> t.setTeacherName(getTeacherName(t.getTeacherId())));
            }
        });
        return classes;
    }

    @Override
    public Class saveClass(Class cl) {
        if (StringUtils.isEmpty(cl.getId())) {
            cl.setActive(true);
            cl.setCreatedAt(new Date());
            cl.setUpdatedAt(new Date());
            classRepository.save(cl);
            Keycloak kc = Keycloak.getInstance("http://localhost:8080/auth",
                    "KidCare", "admin", "admin", "kidcare");

            RoleRepresentation clientRoleRepresentation = new RoleRepresentation();
            clientRoleRepresentation.setName("class" + "_" + cl.getId());
            clientRoleRepresentation.setClientRole(true);
            clientRoleRepresentation.setComposite(false);
            kc.realm("KidCare").clients().get("b544d200-892f-43c7-9500-9408efcfcc5d").roles().create(clientRoleRepresentation);
            return cl;
        }
        return classRepository.save(cl);
    }

    @Override
    public void closeClass(String classId) {
        Class cl = classRepository.findClassById(classId).orElseThrow(() -> new EntityNotFoundException("can not found class"));
        cl.setEndedAt(new Date());
        cl.setActive(false);
        cl.setUpdatedAt(new Date());
        classRepository.save(cl);
    }

    private String getChildName(String childId) {
        Child child = childService.getChildByChildId(childId);
        return child.getName();
    }

    private String getTeacherName(String teacherId) {
        Teacher teacher = teacherService.getTeacherByTeacherId(teacherId);
        return teacher.getName();
    }
}

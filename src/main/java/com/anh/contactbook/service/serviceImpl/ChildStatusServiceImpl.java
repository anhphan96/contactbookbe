package com.anh.contactbook.service.serviceImpl;

import com.anh.contactbook.entity.Child;
import com.anh.contactbook.entity.ChildStatus;
import com.anh.contactbook.entity.Hygience;
import com.anh.contactbook.exception.InvalidParamException;
import com.anh.contactbook.repository.ChildStatusRepository;
import com.anh.contactbook.service.ChildService;
import com.anh.contactbook.service.ChildStatusService;
import com.anh.contactbook.service.ClassService;
import com.anh.contactbook.untility.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.anh.contactbook.entity.Class;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class ChildStatusServiceImpl implements ChildStatusService {

    private final ChildStatusRepository childStatusRepository;

    private final ClassService classService;

    private final ChildService childService;

    @Autowired
    public ChildStatusServiceImpl(ChildStatusRepository childStatusRepository, ClassService classService, ChildService childService) {
        this.childStatusRepository = childStatusRepository;
        this.classService = classService;
        this.childService = childService;
    }

    @Override
    public List<ChildStatus> getChildStatusByCurrentDate(String classId, String createdAt) {
        Class currentClass = classService.getClassByClassId(classId);
        List<ChildStatus> childStatuses = new ArrayList<>();
        currentClass.getChildrenInfoOfClasses().forEach(c -> {
            Date dbCreatedAt = DateUtil.getDataBaseDate(DateUtil.parseStringToDate(createdAt));
            if (childStatusRepository.getChildStatusByChildIdAndCreatedAt(c.getChildId(), dbCreatedAt).isPresent()) {
                childStatuses.add(childStatusRepository.getChildStatusByChildIdAndCreatedAt(c.getChildId(), DateUtil.parseStringToDate(createdAt)).get());
            } else {
                ChildStatus childStatus = new ChildStatus();
                Child child = childService.getChildByChildId(c.getChildId());
                childStatus.setChildId(c.getChildId());
                childStatus.setCreatedAt(dbCreatedAt);
                childStatus.setUpdatedAt(dbCreatedAt);
                childStatus.setChildName(child.getName());
                childStatus.setHygience(new Hygience());
                childStatusRepository.save(childStatus);
                childStatuses.add(childStatus);
            }
        });
        return childStatuses;
    }

    @Override
    public void save(List<ChildStatus> childStatuses) {
        childStatuses.forEach(childStatusRepository::save);
    }

    @Override
    public List<ChildStatus> getIndividualChildStatus(String childId, String date) {
        Date createAtInput = DateUtil.parseStringToDate(date);
        Calendar calBegin = Calendar.getInstance();
        calBegin.setTime(createAtInput);
        calBegin.set(Calendar.DAY_OF_MONTH, calBegin.getActualMinimum(Calendar.DAY_OF_MONTH));

        Calendar calEnd = Calendar.getInstance();
        calEnd.setTime(createAtInput);
        calEnd.set(Calendar.DAY_OF_MONTH, calEnd.getActualMaximum(Calendar.DAY_OF_MONTH));
        calEnd.set(Calendar.HOUR_OF_DAY, 24);
        calEnd.set(Calendar.MINUTE, 1);
        return childStatusRepository.getChildStatusByChildIdAndCreatedAtBetweenOrderByCreatedAtDesc(childId, calBegin.getTime(), calEnd.getTime());
    }
}

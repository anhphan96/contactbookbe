package com.anh.contactbook.service.serviceImpl;

import com.anh.contactbook.entity.ClassStatus;
import com.anh.contactbook.entity.Menu;
import com.anh.contactbook.entity.Schedule;
import com.anh.contactbook.exception.EntityNotFoundException;
import com.anh.contactbook.exception.EntityWasExistedException;
import com.anh.contactbook.repository.ClassStatusRepository;
import com.anh.contactbook.service.ClassStatusService;
import com.anh.contactbook.untility.DateUtil;
import org.joda.time.DateTimeComparator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class ClassStatusServiceImpl implements ClassStatusService {

    private final ClassStatusRepository classStatusRepository;

    @Autowired
    public ClassStatusServiceImpl(ClassStatusRepository classStatusRepository) {
        this.classStatusRepository = classStatusRepository;
    }

    @Override
    public List<ClassStatus> findClassStatusByClassIdAndCreatedAtBetween(String classId, Date createdAt) {
        Calendar calBegin = Calendar.getInstance();
        calBegin.setTime(createdAt);
        calBegin.set(Calendar.DAY_OF_MONTH, calBegin.getActualMinimum(Calendar.DAY_OF_MONTH));

        Calendar calEnd = Calendar.getInstance();
        calEnd.setTime(createdAt);
        calEnd.set(Calendar.DAY_OF_MONTH, calEnd.getActualMaximum(Calendar.DAY_OF_MONTH));
        calEnd.set(Calendar.HOUR_OF_DAY, 24);
        calEnd.set(Calendar.MINUTE, 1);

        return classStatusRepository.findClassStatusByClassIdAndCreatedAtBetween(classId, calBegin.getTime(), calEnd.getTime());
    }

    @Override
    public ClassStatus findClassStatusByClassIdAndCreatedAt(String classId, Date createdAt) {
//        return classStatusRepository.findClassStatusByClassIdAndCreatedAt(classId, createdAt).orElseThrow(() -> new EntityNotFoundException("not found"));
        if (classStatusRepository.findClassStatusByClassIdAndCreatedAt(classId, createdAt).isPresent()) {
            return classStatusRepository.findClassStatusByClassIdAndCreatedAt(classId, createdAt).get();
        }
        throw new EntityNotFoundException("notfound");
    }

    @Override
    public void saveOrCreateClassStatus(ClassStatus classStatus) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY,0);
        cal.set(Calendar.MINUTE,0);
        cal.set(Calendar.SECOND,0);
        cal.set(Calendar.MILLISECOND,0);
        if (Objects.isNull(classStatus.getId())) {
            classStatus.setCreatedAt(cal.getTime());
            classStatus.setUpdatedAt(cal.getTime());
        } else {
            classStatus.setUpdatedAt(cal.getTime());
        }

        classStatusRepository.save(classStatus);
    }

    @Override
    public ClassStatus createMenu(Menu menu, String classId, Date createAt) {
        if (classStatusRepository.findClassStatusByClassIdAndCreatedAt(classId, createAt).isPresent()) {
            ClassStatus classStatus = classStatusRepository.findClassStatusByClassIdAndCreatedAt(classId, createAt).get();
            if (!Objects.isNull(classStatus.getMenu())) {
                throw new EntityWasExistedException("There is a menu in this day!");
            }
            classStatus.setMenu(menu);
            classStatus.setUpdatedAt(DateUtil.getDataBaseDate(new Date()));
            menu.setUpdatedAt(DateUtil.getDataBaseDate(new Date()));
            classStatusRepository.save(classStatus);
            return classStatus;
        }

        ClassStatus classStatus = new ClassStatus();
        classStatus.setClassId(classId);
        classStatus.setMenu(menu);
        classStatus.getMenu().setUpdatedAt(DateUtil.getDataBaseDate(createAt));

        classStatus.setCreatedAt(DateUtil.getDataBaseDate(createAt));
        classStatus.setUpdatedAt(DateUtil.getDataBaseDate(createAt));
        classStatusRepository.save(classStatus);
        return classStatus;
    }

    @Override
    public ClassStatus updateMenu(ClassStatus classStatus, Date createAt) {
        if (DateTimeComparator.getDateOnlyInstance().compare(classStatus.getCreatedAt(), createAt) == 0){
            classStatusRepository.save(classStatus);
            return classStatus;
        }

        if (classStatusRepository.findClassStatusByClassIdAndCreatedAt(classStatus.getClassId(), createAt).isPresent()) {
            ClassStatus newClassStatus = classStatusRepository.findClassStatusByClassIdAndCreatedAt(classStatus.getClassId(), createAt).get();
            if (!Objects.isNull(newClassStatus.getMenu())) {
                throw new EntityWasExistedException("There is a menu in this day!");
            }
            newClassStatus.setMenu(classStatus.getMenu());
            newClassStatus.getMenu().setUpdatedAt(DateUtil.getDataBaseDate(new Date()));
            newClassStatus.setUpdatedAt(DateUtil.getDataBaseDate(new Date()));

            classStatus.setUpdatedAt(DateUtil.getDataBaseDate(new Date()));
            classStatus.setMenu(null);
            classStatusRepository.save(newClassStatus);
            classStatusRepository.save(classStatus);
            return newClassStatus;
        }

        ClassStatus newClassStatus = new ClassStatus();
        newClassStatus.setClassId(classStatus.getClassId());

        newClassStatus.setMenu(classStatus.getMenu());
        newClassStatus.getMenu().setUpdatedAt(DateUtil.getDataBaseDate(createAt));

        newClassStatus.setCreatedAt(DateUtil.getDataBaseDate(createAt));
        newClassStatus.setUpdatedAt(DateUtil.getDataBaseDate(createAt));

        classStatusRepository.save(newClassStatus);

        classStatus.setMenu(null);
        classStatus.setUpdatedAt(DateUtil.getDataBaseDate(new Date()));
        classStatusRepository.save(classStatus);
        return newClassStatus;
    }

    @Override
    public List<ClassStatus> findClassStatusByClassIdInTheWeek(String classId, Date createAt) {
        Calendar calBegin = Calendar.getInstance();
        calBegin.setTime(createAt);
        calBegin.set(Calendar.DAY_OF_WEEK, calBegin.getActualMinimum(Calendar.DAY_OF_WEEK));

        Calendar calEnd = Calendar.getInstance();
        calEnd.setTime(createAt);
        calEnd.set(Calendar.DAY_OF_WEEK, calEnd.getActualMaximum(Calendar.DAY_OF_WEEK));
        calEnd.set(Calendar.HOUR_OF_DAY, 24);
        calEnd.set(Calendar.MINUTE, 1);

        return classStatusRepository.findClassStatusByClassIdAndCreatedAtBetween(classId, calBegin.getTime(), calEnd.getTime());
    }

    @Override
    public ClassStatus createSchedule(Schedule schedule, String classId, Date createAt) {
        if (classStatusRepository.findClassStatusByClassIdAndCreatedAt(classId, createAt).isPresent()) {
            ClassStatus classStatus = classStatusRepository.findClassStatusByClassIdAndCreatedAt(classId, createAt).get();
            if (!Objects.isNull(classStatus.getSchedule())) {
                throw new EntityWasExistedException("There is a schedule in this day!");
            }
            schedule.setUpdatedAt(DateUtil.getDataBaseDate(new Date()));
            classStatus.setSchedule(schedule);
            classStatus.setUpdatedAt(DateUtil.getDataBaseDate(new Date()));
            classStatusRepository.save(classStatus);
            return classStatus;
        }

        ClassStatus classStatus = new ClassStatus();
        classStatus.setClassId(classId);
        classStatus.setSchedule(schedule);
        classStatus.getSchedule().setUpdatedAt(DateUtil.getDataBaseDate(createAt));

        classStatus.setCreatedAt(DateUtil.getDataBaseDate(createAt));
        classStatus.setUpdatedAt(DateUtil.getDataBaseDate(createAt));
        classStatusRepository.save(classStatus);
        return classStatus;
    }

    @Override
    public ClassStatus updateSchedule(ClassStatus classStatus, Date createAtOfSchedule) {
        if (DateTimeComparator.getDateOnlyInstance().compare(classStatus.getCreatedAt(), createAtOfSchedule) == 0){
            classStatusRepository.save(classStatus);
            return classStatus;
        }

        if (classStatusRepository.findClassStatusByClassIdAndCreatedAt(classStatus.getClassId(), createAtOfSchedule).isPresent()) {
            ClassStatus newClassStatus = classStatusRepository.findClassStatusByClassIdAndCreatedAt(classStatus.getClassId(), createAtOfSchedule).get();
            if (!Objects.isNull(newClassStatus.getSchedule())) {
                throw new EntityWasExistedException("There is a schedule in this day!");
            }
            newClassStatus.setSchedule(classStatus.getSchedule());
            newClassStatus.getSchedule().setUpdatedAt(DateUtil.getDataBaseDate(new Date()));
            newClassStatus.setUpdatedAt(DateUtil.getDataBaseDate(new Date()));

            classStatus.setUpdatedAt(DateUtil.getDataBaseDate(new Date()));
            classStatus.setSchedule(null);
            classStatusRepository.save(newClassStatus);
            classStatusRepository.save(classStatus);
            return newClassStatus;
        }

        ClassStatus newClassStatus = new ClassStatus();
        newClassStatus.setClassId(classStatus.getClassId());

        newClassStatus.setSchedule(classStatus.getSchedule());
        newClassStatus.getSchedule().setUpdatedAt(DateUtil.getDataBaseDate(createAtOfSchedule));

        newClassStatus.setCreatedAt(DateUtil.getDataBaseDate(createAtOfSchedule));
        newClassStatus.setUpdatedAt(DateUtil.getDataBaseDate(createAtOfSchedule));

        classStatusRepository.save(newClassStatus);

        classStatus.setSchedule(null);
        classStatus.setUpdatedAt(DateUtil.getDataBaseDate(new Date()));
        classStatusRepository.save(classStatus);
        return newClassStatus;
    }
}

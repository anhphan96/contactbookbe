package com.anh.contactbook.service.serviceImpl;

import com.anh.contactbook.entity.Attendance;
import com.anh.contactbook.entity.Child;
import com.anh.contactbook.entity.ChildrenAttendance;
import com.anh.contactbook.entity.Class;
import com.anh.contactbook.enums.AttendanceStatus;
import com.anh.contactbook.exception.EntityNotFoundException;
import com.anh.contactbook.model.IndividualAttendance;
import com.anh.contactbook.repository.AttendanceRepository;
import com.anh.contactbook.service.AttendanceService;
import com.anh.contactbook.service.ChildService;
import com.anh.contactbook.service.ClassService;
import com.anh.contactbook.untility.DateUtil;
import org.joda.time.DateTimeComparator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class AttendanceServiceImpl implements AttendanceService {

    private final
    AttendanceRepository attendanceRepository;

    private final ClassService classService;

    private final ChildService childService;

    @Autowired
    public AttendanceServiceImpl(AttendanceRepository attendanceRepository,
                                 ClassService classService,
                                 ChildService childService) {
        this.attendanceRepository = attendanceRepository;
        this.classService = classService;
        this.childService = childService;
    }

    @Override
    public Attendance getAttendanceByCreatedDay(String classId, Date createdDay) {
        if (attendanceRepository.findAttendanceByClassIdAndCreatedAt(classId, createdDay).isPresent()) {
            return attendanceRepository.findAttendanceByClassIdAndCreatedAt(classId, createdDay).get();
        }
//        if (DateTimeComparator.getDateOnlyInstance().compare(createdDay, new Date()) == 0) {
//
//        }
        Attendance attendance = new Attendance();
        Class cl = classService.getClassByClassId(classId);
        List<Child> children = childService.getChildrenByClassId(classId);
        List<ChildrenAttendance> childrenAttendances = new ArrayList<>();
        children.stream().forEach(c -> {
            ChildrenAttendance childrenAttendance = new ChildrenAttendance();
            childrenAttendance.setName(c.getName());
            childrenAttendance.setChildId(c.getId());
            childrenAttendance.setBirthDay(c.getBirthDay());
            childrenAttendances.add(childrenAttendance);
        });

        attendance.setCreatedAt(DateUtil.getDataBaseDate(createdDay));
        attendance.setClassId(cl.getId());
        attendance.setChildrenAttendances(childrenAttendances);
        attendanceRepository.save(attendance);
        return attendance;
    }

    @Override
    public void saveOrCreateAttendance(Attendance attendance) {
        attendanceRepository.save(attendance);
    }

    @Override
    public List<IndividualAttendance> getIndividualAttendance(String classId, String childrenId, Date currentDate) {
        Calendar calBegin = Calendar.getInstance();
        calBegin.setTime(currentDate);
        calBegin.set(Calendar.DAY_OF_MONTH, calBegin.getActualMinimum(Calendar.DAY_OF_MONTH));

        Calendar calEnd = Calendar.getInstance();
        calEnd.setTime(new Date());
        calEnd.set(Calendar.HOUR_OF_DAY, 24);
        calEnd.set(Calendar.MINUTE, 1);
        List<Attendance> attendances = attendanceRepository.findAttendanceByClassIdAndCreatedAtBetween(classId, calBegin.getTime(), calEnd.getTime());
        return createIndividualAttendance(childrenId, attendances);
    }

    private List<IndividualAttendance> createIndividualAttendance(String childrenId, List<Attendance> attendances) {
        List<IndividualAttendance> individualAttendances = new ArrayList<>();
        attendances.forEach(a -> {
            IndividualAttendance individualAttendance = new IndividualAttendance();
            List<ChildrenAttendance> childrenAttendances = a.getChildrenAttendances().stream().filter(c ->
                    c.getChildId().equals(childrenId)
            ).collect(Collectors.toList());
            if (childrenAttendances.size() > 0) {
                ChildrenAttendance childrenAttendance = childrenAttendances.get(0);
                individualAttendance.setChildrentId(childrenAttendance.getChildId());
                individualAttendance.setDate(a.getCreatedAt());
                if (childrenAttendance.getAbsent() && childrenAttendance.getPermission()) {
                    individualAttendance.setAttendanceStatus(AttendanceStatus.ALLOWED);
                }else if (childrenAttendance.getAbsent() && !childrenAttendance.getPermission()) {
                    individualAttendance.setAttendanceStatus(AttendanceStatus.NOTALLOWED);
                }else {
                    individualAttendance.setAttendanceStatus(AttendanceStatus.APPEAR);
                }
                individualAttendances.add(individualAttendance);
            }
        });
        return individualAttendances;
    }
}

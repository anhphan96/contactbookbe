package com.anh.contactbook.service.serviceImpl;

import com.anh.contactbook.entity.Class;
import com.anh.contactbook.entity.ClassInfo;
import com.anh.contactbook.entity.Teacher;
import com.anh.contactbook.entity.TeacherInfoOfClass;
import com.anh.contactbook.exception.EntityNotFoundException;
import com.anh.contactbook.exception.InvalidParamException;
import com.anh.contactbook.repository.TeacherRepository;
import com.anh.contactbook.service.ClassService;
import com.anh.contactbook.service.TeacherService;
import com.anh.contactbook.untility.DateUtil;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;

@Service
public class TeacherServiceImpl implements TeacherService {

    private final TeacherRepository teacherRepository;

    private final ClassService classService;

    private Keycloak kc = Keycloak.getInstance("http://localhost:8080/auth",
            "KidCare", "admin", "admin", "kidcare");

    @Autowired
    public TeacherServiceImpl(TeacherRepository teacherRepository, ClassService classService) {
        this.teacherRepository = teacherRepository;
        this.classService = classService;
    }

    @Override
    public Teacher getTeacherByTeacherId(String teacherId) {
        return teacherRepository.findById(teacherId).orElseThrow(() -> new EntityNotFoundException("No teacher found!"));
    }

    @Override
    public List<Teacher> getTeachers() {
        List<Teacher> teachers = teacherRepository.findAllByOrderByCreatedAtDesc();
        teachers.forEach(t -> {
            t.setCurrentClassName(getNameOfClass(t.getCurrentClassId()));
            if (t.getClassInfos() != null) {
                t.getClassInfos().forEach(c -> c.setClassName(getNameOfClass(c.getClassId())));
            }
        });
        return teachers;
    }

    @Override
    public Teacher saveAccount(Teacher teacher) {
        if (StringUtils.isEmpty(teacher.getId())) {
            throw new InvalidParamException("Create information first!");
        }
        Teacher existedTeacher = teacherRepository.findById(teacher.getId()).orElseThrow(() -> new EntityNotFoundException("teacher not found!"));
        if (StringUtils.isEmpty(existedTeacher.getUserName())) {

        }

        if (this.validateAccout(teacher)) {
            if (StringUtils.isEmpty(existedTeacher.getKc_Id())) {
                checkExistUserName(teacher);
                //create user
                UserRepresentation user = new UserRepresentation();
                user.setUsername(teacher.getUserName());
                user.setEnabled(true);
                Response response = kc.realm("KidCare").users().create(user);
                String userId = getCreatedId(response);
                //set password
                UserResource userResource = kc.realm("KidCare").users().get(userId);
                CredentialRepresentation credential = new CredentialRepresentation();
                credential.setValue(teacher.getPassword());
                credential.setType(CredentialRepresentation.PASSWORD);
                credential.setTemporary(true);
                userResource.resetPassword(credential);

                //assign class
                RoleRepresentation savedRoleRepresentation = kc.realm("KidCare").clients()
                        .get("b544d200-892f-43c7-9500-9408efcfcc5d").roles().get("class" + "_" + existedTeacher.getCurrentClassId()).toRepresentation();
                kc.realm("KidCare").users().get(userId).roles().clientLevel("b544d200-892f-43c7-9500-9408efcfcc5d")
                        .add(asList(savedRoleRepresentation));

                existedTeacher.setUserName(teacher.getUserName());
                existedTeacher.setPassword(teacher.getPassword());
                existedTeacher.setKc_Id(userId);
                teacherRepository.save(existedTeacher);
            } else if (!StringUtils.isEmpty(existedTeacher.getUserName())) {
                if (isAccountNotChange(teacher, existedTeacher)) {
                    return existedTeacher;
                }
                UserResource userResource = kc.realm("KidCare").users().get(existedTeacher.getKc_Id());
                CredentialRepresentation credential = new CredentialRepresentation();
                credential.setValue(teacher.getPassword());
                credential.setType(CredentialRepresentation.PASSWORD);
                credential.setTemporary(true);
                userResource.resetPassword(credential);

                existedTeacher.setPassword(teacher.getPassword());
                teacherRepository.save(existedTeacher);
            }
        } else {
            throw new InvalidParamException("Account is invalid!");
        }
        return existedTeacher;
    }

    private boolean isAccountNotChange(Teacher teacher, Teacher existedTeacher) {
        return teacher.getUserName().equals(existedTeacher.getUserName()) &&
                teacher.getPassword().equals(existedTeacher.getPassword());
    }

    private void checkExistUserName(Teacher teacher) {
        List<Teacher> teachers = teacherRepository.findTeacherByUserName(teacher.getUserName());
        if (teachers.size() > 0)
            throw new InvalidParamException("This username is existed!");
    }

    private String getCreatedId(Response response) {
        URI location = response.getLocation();
        if (!response.getStatusInfo().equals(Response.Status.CREATED)) {
            Response.StatusType statusInfo = response.getStatusInfo();
            throw new WebApplicationException("Create method returned status " +
                    statusInfo.getReasonPhrase() + " (Code: " + statusInfo.getStatusCode() + "); expected status: Created (201)", response);
        }
        if (location == null) {
            return null;
        }
        String path = location.getPath();
        return path.substring(path.lastIndexOf('/') + 1);
    }

    @Override
    public Teacher saveTeacher(Teacher teacher) {
        if (StringUtils.isEmpty(teacher.getCurrentClassId())) {
            throw new InvalidParamException("No current class!");
        }
        teacher.setBirthday(DateUtil.plusDate(teacher.getBirthday(), 1));
        if (!StringUtils.isEmpty(teacher.getId())) {
            Teacher existedTeacher = teacherRepository.findById(teacher.getId()).orElseThrow(() -> new EntityNotFoundException("teacher not found!"));
            existedTeacher.setBirthday(teacher.getBirthday());
            existedTeacher.setPhone(teacher.getPhone());
            existedTeacher.setGender(teacher.getGender());
            existedTeacher.setName(teacher.getName());
            existedTeacher.setUpdatedAt(new Date());

            if (!teacher.getCurrentClassId().equals(existedTeacher.getCurrentClassId())) {
                if (Objects.isNull(existedTeacher.getClassInfos())) {
                    existedTeacher.setClassInfos(new ArrayList<>());
                }
                removeTeacherFromClass(existedTeacher);
                addTeacherToClass(teacher.getCurrentClassId(), existedTeacher);
                existedTeacher.setCurrentClassId(teacher.getCurrentClassId());
                existedTeacher.setCurrentClassName(getNameOfClass(teacher.getCurrentClassId()));

                if (existedTeacher.getClassInfos().stream().filter(c -> c.getTo() == null)
                        .collect(Collectors.toList()).size() > 0) {
                    existedTeacher.getClassInfos().stream().filter(c -> c.getTo() == null)
                            .collect(Collectors.toList()).get(0).setTo(new Date());
                }

                existedTeacher.getClassInfos().add(0, new ClassInfo(teacher.getCurrentClassId(), new Date(), null, getNameOfClass(teacher.getCurrentClassId())));
            }

            teacherRepository.save(existedTeacher);
            return existedTeacher;
        }
        if (Objects.isNull(teacher.getClassInfos())) {
            teacher.setClassInfos(new ArrayList<>());
        } else {
            throw new InvalidParamException("data is invalid!");
        }
        teacher.setCurrentClassName(getNameOfClass(teacher.getCurrentClassId()));
        teacher.getClassInfos().add(0, new ClassInfo(teacher.getCurrentClassId(), new Date(), null, getNameOfClass(teacher.getCurrentClassId())));
        teacher.setCreatedAt(new Date());
        teacherRepository.save(teacher);
        addTeacherToClass(teacher.getCurrentClassId(), teacher);
        return teacher;
    }

    @Override
    public Teacher getTeacherByAccount(String username) {
        List<Teacher> teacher = teacherRepository.findTeacherByUserName(username);
        if (teacher.size() > 0)
            return teacher.get(0);
        throw new EntityNotFoundException("No teacher found!");
    }

    private String getNameOfClass(String classID) {
        Class cl = classService.getClassByClassId(classID);
        return cl.getName();
    }

    private void addTeacherToClass(String classId, Teacher teacher) {
        Class cl = classService.getClassByClassId(classId);
        if (cl.getTeacherInfoOfClasses() != null) {
            cl.getTeacherInfoOfClasses().add(0, new TeacherInfoOfClass(teacher.getId(), teacher.getName()));
        } else {
            cl.setTeacherInfoOfClasses(new ArrayList<>());
            cl.getTeacherInfoOfClasses().add(new TeacherInfoOfClass(teacher.getId(), teacher.getName()));
        }
        if (!StringUtils.isEmpty(teacher.getKc_Id()) &&
                !StringUtils.isEmpty(teacher.getUserName())) {
            RoleRepresentation savedRoleRepresentation = kc.realm("KidCare").clients()
                    .get("b544d200-892f-43c7-9500-9408efcfcc5d").roles().get("class" + "_" + classId).toRepresentation();
            kc.realm("KidCare").users().get(teacher.getKc_Id()).roles().clientLevel("b544d200-892f-43c7-9500-9408efcfcc5d")
                    .add(asList(savedRoleRepresentation));
        }

        classService.saveClass(cl);
    }

    private void removeTeacherFromClass(Teacher teacher) {
        Class cl = classService.getClassByClassId(teacher.getCurrentClassId());
        if (cl.getTeacherInfoOfClasses() != null) {
            cl.getTeacherInfoOfClasses().removeIf(t -> t.getTeacherId().equals(teacher.getId()));
        }
        if (!StringUtils.isEmpty(teacher.getKc_Id()) &&
                !StringUtils.isEmpty(teacher.getUserName())) {
            RoleRepresentation savedRoleRepresentation = kc.realm("KidCare").clients()
                    .get("b544d200-892f-43c7-9500-9408efcfcc5d").roles().get("class" + "_" + cl.getId()).toRepresentation();
            kc.realm("KidCare").users().get(teacher.getKc_Id()).roles().clientLevel("b544d200-892f-43c7-9500-9408efcfcc5d")
                    .remove(asList(savedRoleRepresentation));
        }

        classService.saveClass(cl);
    }

    private boolean validateAccout(Teacher teacher) {
        if (StringUtils.isEmpty(teacher.getUserName()))
            return false;
        if (StringUtils.isEmpty(teacher.getPassword()))
            return false;
        return true;
    }
}

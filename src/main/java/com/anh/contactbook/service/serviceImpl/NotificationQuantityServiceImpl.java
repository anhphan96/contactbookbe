package com.anh.contactbook.service.serviceImpl;

import com.anh.contactbook.model.NotificationQuantity;
import com.anh.contactbook.service.ApplicationService;
import com.anh.contactbook.service.NotificationQuantityService;
import com.anh.contactbook.service.PrescriptionService;
import com.anh.contactbook.service.SystemLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NotificationQuantityServiceImpl implements NotificationQuantityService {

    private final ApplicationService applicationService;
    private final PrescriptionService prescriptionService;
    private final SystemLogService systemLogService;

    @Autowired
    public NotificationQuantityServiceImpl(ApplicationService applicationService, PrescriptionService prescriptionService, SystemLogService systemLogService) {
        this.applicationService = applicationService;
        this.prescriptionService = prescriptionService;
        this.systemLogService = systemLogService;
    }

    @Override
    public NotificationQuantity getNotificationQuantityOfParent(String classId, String childId) {
        NotificationQuantity notificationQuantity = new NotificationQuantity();
        notificationQuantity.setApplication(applicationService.countNotification(classId, childId));
        notificationQuantity.setPrescription(prescriptionService.countNotification(childId));
        notificationQuantity.setSystemlog(systemLogService.countNotification(childId));
        return notificationQuantity;
    }

    @Override
    public NotificationQuantity getNotificationQuantityOfTeacher(String classId) {
        NotificationQuantity notificationQuantity = new NotificationQuantity();
        notificationQuantity.setApplication(applicationService.countTeacherNotification(classId));
        notificationQuantity.setPrescription(prescriptionService.countTeacherNotification(classId));
        return notificationQuantity;
    }
}

package com.anh.contactbook.service.serviceImpl;

import com.anh.contactbook.entity.*;
import com.anh.contactbook.entity.Class;
import com.anh.contactbook.exception.EntityNotFoundException;
import com.anh.contactbook.exception.InvalidParamException;
import com.anh.contactbook.repository.ChildRepository;
import com.anh.contactbook.service.ChildService;
import com.anh.contactbook.service.ClassService;
import com.anh.contactbook.service.ParentsService;
import com.anh.contactbook.untility.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class ChildServiceImpl implements ChildService {

    private final ChildRepository childRepository;

    private final ClassService classService;

    private final ParentsService parentsService;

    @Autowired
    public ChildServiceImpl(ChildRepository childRepository, ClassService classService, ParentsService parentsService) {
        this.childRepository = childRepository;
        this.classService = classService;
        this.parentsService = parentsService;
    }

    @Override
    public List<Child> getChildrenByClassId(String classId) {
        List<Child> children = childRepository.findChildByCurrentClassId(classId);
        if (children.isEmpty()) {
            throw new EntityNotFoundException("No Children found in this class");
        }
        return children;
    }

    @Override
    public Child getChildByChildId(String childId) {
        return childRepository.findById(childId).orElseThrow(() -> new EntityNotFoundException("Can not find Child!"));
    }

    @Override
    public List<Child> getChildren() {
        List<Child> children = childRepository.findAllByOrderByCreatedAtDesc();
        children.forEach(c -> {
            c.setCurrentClassName(getNameOfClass(c.getCurrentClassId()));
            if (c.getClassInfos() != null) {
                c.getClassInfos().forEach(cl -> cl.setClassName(getNameOfClass(cl.getClassId())));
            }
        });
        return children;
    }

    @Override
    public Child saveChild(Child child) {
        if (StringUtils.isEmpty(child.getCurrentClassId())) {
            throw new InvalidParamException("No current class!");
        }
        int age = DateUtil.getMonthAge(child.getBirthDay());
        if (age <= 0) {
            throw new InvalidParamException("The age has to greater than 1 month age!");
        }
        if (!StringUtils.isEmpty(child.getId())) {
            Child existedChild = childRepository.findById(child.getId()).orElseThrow(() -> new EntityNotFoundException("teacher not found!"));

            existedChild.setBirthDay(child.getBirthDay());
            existedChild.setGender(child.getGender());
            existedChild.setName(child.getName());
            existedChild.setUpdatedAt(new Date());
            existedChild.setCurrentClassName(getNameOfClass(child.getCurrentClassId()));
            if (!child.getCurrentClassId().equals(existedChild.getCurrentClassId())) {
                if (Objects.isNull(existedChild.getClassInfos())) {
                    existedChild.setClassInfos(new ArrayList<>());
                }
                if (!StringUtils.isEmpty(existedChild.getCurrentClassId())) {
                    removeChildFromClass(existedChild.getCurrentClassId(), child.getId());
                }
                addChildToClass(child.getCurrentClassId(), existedChild);
                existedChild.setCurrentClassId(child.getCurrentClassId());

                existedChild.getClassInfos().stream().filter(c -> c.getTo() == null)
                        .collect(Collectors.toList()).get(0).setTo(new Date());

                existedChild.getClassInfos().add(0, new ClassInfo(child.getCurrentClassId(), new Date(), null, getNameOfClass(child.getCurrentClassId())));
            }

            if(!StringUtils.isEmpty(child.getParentsId()) && !StringUtils.isEmpty(existedChild.getParentsId())) {
                if (!child.getParentsId().equals(existedChild.getParentsId())) {
                    removeChildFromParents(existedChild.getParentsId(), existedChild.getId());
                    addChildToParents(child.getParentsId(), existedChild);
                    existedChild.setParentsId(child.getParentsId());
                }
            }

            existedChild.setAge(DateUtil.getMonthAge(child.getBirthDay()));
            child.setBirthDay(DateUtil.plusDate(child.getBirthDay(), 1));
            childRepository.save(existedChild);
            return existedChild;
        }
        if (Objects.isNull(child.getClassInfos())) {
            child.setClassInfos(new ArrayList<>());
        } else {
            throw new InvalidParamException("data is invalid!");
        }
        child.setCurrentClassName(getNameOfClass(child.getCurrentClassId()));
        child.getClassInfos().add(0, new ClassInfo(child.getCurrentClassId(), new Date(), null, getNameOfClass(child.getCurrentClassId())));
        child.setCreatedAt(new Date());
        child.setAge(DateUtil.getMonthAge(child.getBirthDay()));
        child.setBirthDay(DateUtil.plusDate(child.getBirthDay(), 1));
        childRepository.save(child);
        addChildToClass(child.getCurrentClassId(), child);
        if (!StringUtils.isEmpty(child.getParentsId())) {
            addChildToParents(child.getParentsId(), child);
        }
        return child;
    }

    @Override
    public List<Child> getChildrenByParents(String parentsUserName) {
        Parents parents = parentsService.getParentsByUserName(parentsUserName);
        return childRepository.findChildByParentsId(parents.getId());
    }

    private void addChildToClass(String classId, Child child) {
        Class cl = classService.getClassByClassId(classId);
        if (cl.getChildrenInfoOfClasses() != null) {
            cl.getChildrenInfoOfClasses().add(0, new ChildrenInfoOfClass(child.getId(), child.getName(), child.getBirthDay()));
        } else {
            cl.setChildrenInfoOfClasses(new ArrayList<>());
            cl.getChildrenInfoOfClasses().add(new ChildrenInfoOfClass(child.getId(), child.getName(), child.getBirthDay()));
        }
        classService.saveClass(cl);
    }

    private void addChildToParents(String parentsId, Child child) {
        Parents pa = parentsService.getParentsById(parentsId);
        if (pa.getChildrenInfoOfClasses() != null) {
            pa.getChildrenInfoOfClasses().add(0, new ChildrenInfoOfClass(child.getId(), child.getName(), child.getBirthDay()));
        } else {
            pa.setChildrenInfoOfClasses(new ArrayList<>());
            pa.getChildrenInfoOfClasses().add(new ChildrenInfoOfClass(child.getId(), child.getName(), child.getBirthDay()));
        }
        parentsService.saveParents(pa);
    }

    private void removeChildFromClass(String classId, String childId) {
        Class cl = classService.getClassByClassId(classId);
        if (cl.getChildrenInfoOfClasses() != null) {
            cl.getChildrenInfoOfClasses().removeIf(t -> t.getChildId().equals(childId));
        }
        classService.saveClass(cl);
    }

    private void removeChildFromParents(String parentsId, String childId) {
        Parents pa = parentsService.getParentsById(parentsId);
        if (pa.getChildrenInfoOfClasses() != null) {
            pa.getChildrenInfoOfClasses().removeIf(t -> t.getChildId().equals(childId));
        }
        parentsService.saveParents(pa);
    }

    private String getNameOfClass(String classID) {
        Class cl = classService.getClassByClassId(classID);
        return cl.getName();
    }
}

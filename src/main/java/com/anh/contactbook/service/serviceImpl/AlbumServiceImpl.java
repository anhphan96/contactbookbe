package com.anh.contactbook.service.serviceImpl;

import com.anh.contactbook.controller.WebSockerController;
import com.anh.contactbook.entity.Album;
import com.anh.contactbook.entity.Child;
import com.anh.contactbook.entity.Image;
import com.anh.contactbook.entity.SystemLog;
import com.anh.contactbook.exception.EntityNotFoundException;
import com.anh.contactbook.exception.InvalidParamException;
import com.anh.contactbook.repository.AlbumRepository;
import com.anh.contactbook.service.AlbumService;
import com.anh.contactbook.service.ChildService;
import com.anh.contactbook.service.SystemLogService;
import com.anh.contactbook.untility.DateUtil;
import com.anh.contactbook.untility.PhotoUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class AlbumServiceImpl implements AlbumService {

    private final AlbumRepository albumRepository;

    private final SystemLogService systemLogService;

    private final WebSockerController webSockerController;

    private final ChildService childService;

    @Autowired
    public AlbumServiceImpl(AlbumRepository albumRepository, SystemLogService systemLogService, WebSockerController webSockerController, ChildService childService) {
        this.albumRepository = albumRepository;
        this.systemLogService = systemLogService;
        this.webSockerController = webSockerController;
        this.childService = childService;
    }

    @Override
    public Album createAlbum(Album album) {
        if (album.getTitle().isEmpty()) {
            throw new InvalidParamException("Album name is require!");
        }
        album.setCreatedAt(DateUtil.getDataBaseDate(new Date()));
        album.setUpdatedAt(DateUtil.getDataBaseDate(new Date()));
        albumRepository.save(album);

        List<Child> children = childService.getChildrenByClassId(album.getClassId());
        SystemLog systemLog = new SystemLog(new Date(),
                "Teacher have created " + album.getTitle() + " album",
                "photo_album", "",
                false);
        children.forEach(c -> {
            systemLog.setId(null);
            systemLog.setDynamicObject(album);
            systemLog.setChildId(c.getId());
            systemLogService.saveSystemLog(systemLog);
        });
        webSockerController.confirmApplicationLog("album", album.getClassId(), systemLog);
        return album;
    }

    @Override
    public Album getAlbumByAlbumId(String albumId) {
        Album album = albumRepository.getAlbumById(albumId).orElseThrow(() -> new EntityNotFoundException("No albums found!"));
        return album;
    }

    @Override
    public Album createPhotos(Album album) {
        Album existedAlbum = albumRepository.getAlbumById(album.getId()).orElseThrow(() -> new EntityNotFoundException("No albums found!"));
        album.getImages().forEach(i -> {
            Image newImage = new Image();
            try {
                newImage.setUrl(PhotoUtil.saveImage(i.getBase64().split(",")[1]));
            } catch (Exception e) {
                throw new InvalidParamException("Image is invalid!");
            }
            if (Objects.isNull(existedAlbum.getImages())) {
                existedAlbum.setImages(new ArrayList<>());
                existedAlbum.getImages().add(newImage);
            } else {
                existedAlbum.getImages().add(newImage);
            }
        });
        albumRepository.save(existedAlbum);
        return existedAlbum;
    }

    @Override
    public List<Album> getAlbumsByClassId(String classId) {
        return albumRepository.getAlbumByClassIdOrderByCreatedAtDesc(classId);
    }

    @Override
    public Album updateAlbum(Album album) {
        Album updateAlbum = albumRepository.getAlbumById(album.getId()).orElseThrow(() -> new EntityNotFoundException("No albums found!"));
        updateAlbum.setUpdatedAt(DateUtil.getDataBaseDate(new Date()));
        updateAlbum.setTitle(album.getTitle());
        this.albumRepository.save(updateAlbum);
        return updateAlbum;
    }

    @Override
    public void deleteAlbum(String albumId) {
        Album delAlbum = albumRepository.getAlbumById(albumId).orElseThrow(() -> new EntityNotFoundException("No albums found!"));
        this.albumRepository.delete(delAlbum);
    }
}

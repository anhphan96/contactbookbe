package com.anh.contactbook.service.serviceImpl;

import com.anh.contactbook.controller.WebSockerController;
import com.anh.contactbook.entity.Application;
import com.anh.contactbook.entity.Attendance;
import com.anh.contactbook.entity.Child;
import com.anh.contactbook.entity.SystemLog;
import com.anh.contactbook.exception.InvalidParamException;
import com.anh.contactbook.repository.ApplicationRepository;
import com.anh.contactbook.service.ApplicationService;
import com.anh.contactbook.service.AttendanceService;
import com.anh.contactbook.service.ChildService;
import com.anh.contactbook.service.SystemLogService;
import com.anh.contactbook.untility.DateUtil;
import org.joda.time.DateTimeComparator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ApplicationServiceImpl implements ApplicationService {

    private final ApplicationRepository applicationRepository;

    private final AttendanceService attendanceService;

    private final WebSockerController webSockerController;

    private final SystemLogService systemLogService;

    private final ChildService childService;

    @Autowired
    public ApplicationServiceImpl(ApplicationRepository applicationRepository, AttendanceService attendanceService, WebSockerController webSockerController, SystemLogService systemLogService, ChildService childService) {
        this.applicationRepository = applicationRepository;
        this.attendanceService = attendanceService;
        this.webSockerController = webSockerController;
        this.systemLogService = systemLogService;
        this.childService = childService;
    }

    @Override
    public Application saveApplication(Application application) {
        if (DateTimeComparator.getDateOnlyInstance().compare(application.getFromDate(), application.getToDate()) == 1) {
            throw new InvalidParamException("From day must before to day");
        }
        Child child = childService.getChildByChildId(application.getChildId());
        application.setCreatedAt(DateUtil.getDataBaseDate(DateUtil.plusDate(new Date(), 1)));
        application.setConfirm(false);
        application.setFromDate(DateUtil.getDataBaseDate(application.getFromDate()));
        application.setToDate(DateUtil.getDataBaseDate(application.getToDate()));
        this.applicationRepository.save(application);
        SystemLog systemLog = new SystemLog(new Date(),
                child.getName()+ " has a new absent application: " + application.getReason(),
                "sentiment_very_dissatisfied", null,
                false);
        systemLog.setClassId(application.getClassId());
        application.setChildName(child.getName());
        systemLog.setDynamicObject(application);
        systemLogService.saveSystemLog(systemLog);
        webSockerController.confirmApplicationLog("application", application.getClassId(), systemLog);
        return application;
    }

    @Override
    public void confirmApplication(Application application) {
        Date date = DateUtil.getDataBaseDate(application.getFromDate());
        if (DateTimeComparator.getDateOnlyInstance().compare(date, application.getToDate()) == 0) {
            Attendance attendance = attendanceService.getAttendanceByCreatedDay(application.getClassId(), date);
            attendance.getChildrenAttendances().stream()
                    .filter(c -> c.getChildId().equals(application.getChildId())).collect(Collectors.toList())
                    .get(0).setAbsent(true);
            attendance.getChildrenAttendances().stream()
                    .filter(c -> c.getChildId().equals(application.getChildId())).collect(Collectors.toList())
                    .get(0).setPermission(true);
            attendanceService.saveOrCreateAttendance(attendance);
            application.setConfirm(true);
            applicationRepository.save(application);
            SystemLog systemLog = new SystemLog(new Date(),
                    "Your absent application is confirmed by teacher!",
                    "sentiment_very_dissatisfied", application.getChildId(),
                    false);
            systemLog.setDynamicObject(application.getId());
            systemLogService.saveSystemLog(systemLog);
            webSockerController.confirmApplicationLog("application", application.getChildId(), systemLog);
            return;
        }

        while (DateTimeComparator.getDateOnlyInstance().compare(date, application.getToDate()) != 0) {
            Attendance attendance = attendanceService.getAttendanceByCreatedDay(application.getClassId(), date);
            attendance.getChildrenAttendances().stream()
                    .filter(c -> c.getChildId().equals(application.getChildId())).collect(Collectors.toList())
                    .get(0).setAbsent(true);
            attendance.getChildrenAttendances().stream()
                    .filter(c -> c.getChildId().equals(application.getChildId())).collect(Collectors.toList())
                    .get(0).setPermission(true);
            attendanceService.saveOrCreateAttendance(attendance);
            date = DateUtil.plusDate(date, 1);
        }

        Attendance attendance = attendanceService.getAttendanceByCreatedDay(application.getClassId(), date);
        attendance.getChildrenAttendances().stream()
                .filter(c -> c.getChildId().equals(application.getChildId())).collect(Collectors.toList())
                .get(0).setAbsent(true);
        attendance.getChildrenAttendances().stream()
                .filter(c -> c.getChildId().equals(application.getChildId())).collect(Collectors.toList())
                .get(0).setPermission(true);
        attendanceService.saveOrCreateAttendance(attendance);
        application.setConfirm(true);
        applicationRepository.save(application);
        SystemLog systemLog = new SystemLog(new Date(),
                "Your absent application is confirmed by teacher!",
                "sentiment_very_dissatisfied",
                application.getChildId(),
                false);
        systemLog.setDynamicObject(application.getId());
        systemLogService.saveSystemLog(systemLog);
        webSockerController.confirmApplicationLog("application", application.getChildId(), systemLog);
    }

    @Override
    public List<Application> getApplicationByClassIdAndCreatedAt(String classID) {
        List<Application> applications = applicationRepository.findApplicationByClassIdOrderByCreatedAtDesc(classID);
        applications.forEach(a -> a.setChildName(getChildName(a.getChildId())));
        return applications;
    }

    @Override
    public List<Application> getApplicationByClassIdAndChildId(String classId, String childId) {
        List<Application> applications = applicationRepository.findApplicationByClassIdAndChildIdOrderByCreatedAtDesc(classId, childId);
        applications.forEach(a -> a.setChildName(getChildName(a.getChildId())));
        return applications;
    }

    @Override
    public int countNotification(String classId, String childId) {
        return applicationRepository.countApplicationByClassIdAndChildIdAndConfirmFalse(classId, childId);
    }

    @Override
    public int countTeacherNotification(String classId) {
        return applicationRepository.countApplicationByClassIdAndConfirmFalse(classId);
    }

    private String getChildName(String childId) {
        Child child = childService.getChildByChildId(childId);
        return child.getName();
    }
}

package com.anh.contactbook.service.serviceImpl;

import com.anh.contactbook.entity.Child;
import com.anh.contactbook.entity.Parents;
import com.anh.contactbook.exception.EntityNotFoundException;
import com.anh.contactbook.exception.InvalidParamException;
import com.anh.contactbook.repository.ParentsRepository;
import com.anh.contactbook.service.ChildService;
import com.anh.contactbook.service.ParentsService;
import com.anh.contactbook.untility.DateUtil;
import javafx.scene.Parent;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;

@Service
public class ParentsServiceImpl implements ParentsService {

    private final ParentsRepository parentsRepository;

    private final ChildService childService;

    private Keycloak kc = Keycloak.getInstance("http://localhost:8080/auth",
            "KidCare", "admin", "admin", "kidcare");

    @Autowired
    public ParentsServiceImpl(ParentsRepository parentsRepository, @Lazy ChildService childService) {
        this.parentsRepository = parentsRepository;
        this.childService = childService;
    }

    @Override
    public Parents getParentsByUserName(String username) {
        List<Parents> parents = parentsRepository.findParentsByUserName(username);
        if (parents.size() > 0) {
            return parents.get(0);
        }
        throw new EntityNotFoundException("No Parents found!");
    }

    @Override
    public Parents saveParents(Parents parents) {
        parents.setBirthDay(DateUtil.plusDate(parents.getBirthDay(), 1));
        parentsRepository.save(parents);
        return parents;
    }

    @Override
    public Parents saveAccount(Parents parents) {
        if (StringUtils.isEmpty(parents.getId())) {
            throw new InvalidParamException("Create information first!");
        }
        Parents existedParents = parentsRepository.findById(parents.getId()).orElseThrow(() -> new EntityNotFoundException("Parent not found!"));

        if (this.validateAccout(parents)) {
            if (StringUtils.isEmpty(existedParents.getKc_Id())) {
                checkExistUserName(parents);
                //create user
                UserRepresentation user = new UserRepresentation();
                user.setUsername(parents.getUserName());
                user.setEnabled(true);
                Response response = kc.realm("KidCare").users().create(user);
                String userId = getCreatedId(response);
                //set password
                UserResource userResource = kc.realm("KidCare").users().get(userId);
                CredentialRepresentation credential = new CredentialRepresentation();
                credential.setValue(parents.getPassword());
                credential.setType(CredentialRepresentation.PASSWORD);
                credential.setTemporary(true);
                userResource.resetPassword(credential);

                existedParents.setUserName(parents.getUserName());
                existedParents.setKc_Id(userId);
                parentsRepository.save(existedParents);
            } else if (!StringUtils.isEmpty(existedParents.getUserName())) {
                if (isAccountNotChange(parents, existedParents)) {
                    return existedParents;
                }
                UserResource userResource = kc.realm("KidCare").users().get(existedParents.getKc_Id());
                CredentialRepresentation credential = new CredentialRepresentation();
                credential.setValue(parents.getPassword());
                credential.setType(CredentialRepresentation.PASSWORD);
                credential.setTemporary(true);
                userResource.resetPassword(credential);

                existedParents.setPassword(parents.getPassword());
                parentsRepository.save(existedParents);
            }
        } else {
            throw new InvalidParamException("Account is invalid!");
        }
        return existedParents;
    }

    @Override
    public List<Parents> getParents() {
        List<Parents> parents = parentsRepository.findAllByOrderByCreatedAtDesc();
        parents.forEach(t -> {
            if (t.getChildrenInfoOfClasses() != null) {
                t.getChildrenInfoOfClasses().forEach(c -> c.setChildName(getChildName(c.getChildId())));
            }
        });
        return parents;
    }

    @Override
    public Parents getParentsById(String parentsId) {
        return parentsRepository.findById(parentsId).orElseThrow(() -> new EntityNotFoundException("No parents found!"));
    }

    private boolean isAccountNotChange(Parents parents, Parents existedParents) {
        return parents.getUserName().equals(existedParents.getUserName()) &&
                parents.getPassword().equals(existedParents.getPassword());
    }

    private boolean validateAccout(Parents pa) {
        if (StringUtils.isEmpty(pa.getUserName()))
            return false;
        if (StringUtils.isEmpty(pa.getPassword()))
            return false;
        return true;
    }

    private void checkExistUserName(Parents pa) {
        List<Parents> parents = parentsRepository.findParentsByUserName(pa.getUserName());
        if (parents.size() > 0)
            throw new InvalidParamException("This username is existed!");
    }

    private String getCreatedId(Response response) {
        URI location = response.getLocation();
        if (!response.getStatusInfo().equals(Response.Status.CREATED)) {
            Response.StatusType statusInfo = response.getStatusInfo();
            throw new WebApplicationException("Create method returned status " +
                    statusInfo.getReasonPhrase() + " (Code: " + statusInfo.getStatusCode() + "); expected status: Created (201)", response);
        }
        if (location == null) {
            return null;
        }
        String path = location.getPath();
        return path.substring(path.lastIndexOf('/') + 1);
    }

    private String getChildName(String childId) {
        Child child = childService.getChildByChildId(childId);
        return child.getName();
    }
}

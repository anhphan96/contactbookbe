package com.anh.contactbook.service;

import com.anh.contactbook.entity.Application;

import java.util.Date;
import java.util.List;

public interface ApplicationService {
    Application saveApplication(Application application);
    void confirmApplication(Application application);
    List<Application> getApplicationByClassIdAndCreatedAt(String classID);
    List<Application> getApplicationByClassIdAndChildId(String classId, String childId);
    int countNotification(String classId, String childId);
    int countTeacherNotification(String classId);
}

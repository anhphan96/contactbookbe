package com.anh.contactbook.excetionController;

import com.anh.contactbook.exception.AuthenticationFailException;
import com.anh.contactbook.exception.EntityNotFoundException;
import com.anh.contactbook.exception.EntityWasExistedException;
import com.anh.contactbook.exception.InvalidParamException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionController {

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity handleEntityNotFoundException(EntityNotFoundException ex) {
        return new ResponseEntity<>(ex.getErrorMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(InvalidParamException.class)
    public ResponseEntity handleInvalidParamExceptionException(InvalidParamException ex) {
        return new ResponseEntity<>(ex.getErrorMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(EntityWasExistedException.class)
    public ResponseEntity handleEntityWasExistedException(EntityWasExistedException ex) {
        return new ResponseEntity<>(ex.getErrorMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(AuthenticationFailException.class)
    public ResponseEntity handleAuthenticationFailException(AuthenticationFailException ex) {
        return new ResponseEntity<>("unauthorized", HttpStatus.UNAUTHORIZED);
    }
}

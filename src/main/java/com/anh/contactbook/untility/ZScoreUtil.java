package com.anh.contactbook.untility;

import com.anh.contactbook.enums.Gender;

public class ZScoreUtil {
//    public static double getZScore(String gender, int age, String type, double data) {
//        Double[] lms = new Double[3];
//        if (gender.equals(Gender.MALE)) {
//            if (type.equals("weight")) {
//                lms = getMaleWeightData(age);
//            }else {
//                lms = getMaleHeightData(age);
//            }
//        }else if (gender.equals("FEMALE")) {
//            if (type.equals("weight")) {
//                lms = getFemaleWeightData(age);
//            }else {
//                lms = getFemaleHeightData(age);
//            }
//        }
//        double Z = (Math.pow((data/lms[1]),lms[0]) - 1) / (lms[0]*lms[2]);
//        return (double)(Math.round(Z*1000))/1000;
//    }

    public static double getStandard(String gender, int age, String typeHealth, String typeStandard) {
        Double[] lms = new Double[3];
        if (gender.equals(Gender.MALE.name())) {
            if (typeHealth.equals("weight")) {
                lms = getMaleWeightData(age);
            }else {
                lms = getMaleHeightData(age);
            }
        }else if (gender.equals("FEMALE")) {
            if (typeHealth.equals("weight")) {
                lms = getFemaleWeightData(age);
            }else {
                lms = getFemaleHeightData(age);
            }
        }
        double standard;
        if (typeStandard.equals("high")) {
            standard = Math.pow(2.5 * lms[0] * lms[2] + 1, 1 / (lms[0])) * lms[1];
        }else {
            standard = Math.pow(-2.5 * lms[0] * lms[2] + 1, 1 / (lms[0])) * lms[1];
        }
        return (double)(Math.round(standard*1000))/1000;
    }

    private static Double[] getMaleHeightData(int age) {
        int AgeIndex = getAgeIndex(age) * 4;
        Double[] lms = new Double[3];
        lms[0] = ZScoreDataSource.maleHeight[AgeIndex + 1];
        lms[1] = ZScoreDataSource.maleHeight[AgeIndex + 2];
        lms[2] = ZScoreDataSource.maleHeight[AgeIndex + 3];
        return lms;
    }

    private static Double[] getMaleWeightData(int age) {
        int AgeIndex = getAgeIndex(age) * 4;
        Double[] lms = new Double[3];
        lms[0] = ZScoreDataSource.maleWeight[AgeIndex + 1];
        lms[1] = ZScoreDataSource.maleWeight[AgeIndex + 2];
        lms[2] = ZScoreDataSource.maleWeight[AgeIndex + 3];
        return lms;
    }

    private static Double[] getFemaleHeightData(int age) {
        int AgeIndex = getAgeIndex(age) * 4;
        Double[] lms = new Double[3];
        lms[0] = ZScoreDataSource.femaleHeight[AgeIndex + 1];
        lms[1] = ZScoreDataSource.femaleHeight[AgeIndex + 2];
        lms[2] = ZScoreDataSource.femaleHeight[AgeIndex + 3];
        return lms;
    }

    private static Double[] getFemaleWeightData(int age) {
        int AgeIndex = getAgeIndex(age)*4;
        Double[] lms = new Double[3];
        lms[0] = ZScoreDataSource.femaleWeight[AgeIndex + 1];
        lms[1] = ZScoreDataSource.femaleWeight[AgeIndex + 2];
        lms[2] = ZScoreDataSource.femaleWeight[AgeIndex + 3];
        return lms;
    }

    private static int getAgeIndex(int age) {
        if (age == 0) {
            return 0;
        }
        if (age == 240) {
            return 241;
        }
        return 1 + age;
    }
}

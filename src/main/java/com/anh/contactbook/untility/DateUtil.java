package com.anh.contactbook.untility;

import com.anh.contactbook.exception.InvalidParamException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.text.ParseException;

public class DateUtil {
    public static Date parseStringToDate(String date) {
        String pattern = "dd-MM-yyyy";
        Date parseDate = null;
        try {
            DateFormat df = new SimpleDateFormat(pattern);
            parseDate = df.parse(date);
        } catch (ParseException e) {
            throw new InvalidParamException("Request Params is invalid!");
        }
        return parseDate;
    }

    public static Date getDataBaseDate(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY,0);
        cal.set(Calendar.MINUTE,0);
        cal.set(Calendar.SECOND,0);
        cal.set(Calendar.MILLISECOND,0);
        return cal.getTime();
    }

    public static Date plusDate(Date date, int increase) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, increase);
        return cal.getTime();
    }

    public static int getMonthAge(Date birthDay) {
        Calendar calBirthDate = Calendar.getInstance();
        calBirthDate.setTime(birthDay);

        Date now = new Date();
        Calendar calNow = Calendar.getInstance();
        calNow.setTime(now);
        int difMonth = (calNow.get(Calendar.YEAR) - calBirthDate.get(Calendar.YEAR)) * 12
                + (calNow.get(Calendar.MONTH) - calBirthDate.get(Calendar.MONTH));
        if (calNow.get(Calendar.DAY_OF_MONTH) - calBirthDate.get(Calendar.DAY_OF_MONTH) >= 0)
            return difMonth;
        return difMonth - 1;
    }
}

package com.anh.contactbook.untility;

import com.anh.contactbook.exception.InvalidParamException;
import sun.misc.BASE64Decoder;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.UUID;

public class PhotoUtil {
    private static final String UPLOAD_PATH = "E:\\xxamp\\htdocs\\kidcare\\";

    public static String saveImage(String base64Image)  {
        String imageName = UUID.randomUUID().toString() + ".jpg";
        BufferedImage image;
        byte[] imageByte;
        try {
            BASE64Decoder decoder = new BASE64Decoder();
            imageByte = decoder.decodeBuffer(base64Image);
            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
            image = ImageIO.read(bis);
            bis.close();
            File outputfile = new File(UPLOAD_PATH + imageName);
            ImageIO.write(image, "jpg", outputfile);
            return imageName;
        } catch (Exception e) {
            throw new InvalidParamException("The image is invalid!");
        }
    }
}

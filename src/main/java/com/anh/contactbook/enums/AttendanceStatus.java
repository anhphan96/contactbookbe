package com.anh.contactbook.enums;

public enum  AttendanceStatus {
    APPEAR,
    ALLOWED,
    NOTALLOWED
}

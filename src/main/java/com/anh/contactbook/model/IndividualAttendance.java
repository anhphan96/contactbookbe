package com.anh.contactbook.model;

import com.anh.contactbook.enums.AttendanceStatus;

import java.util.Date;

public class IndividualAttendance {
    private String childrentId;
    private String classId;
    private Date date;
    private AttendanceStatus attendanceStatus;

    public String getChildrentId() {
        return childrentId;
    }

    public void setChildrentId(String childrentId) {
        this.childrentId = childrentId;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public AttendanceStatus getAttendanceStatus() {
        return attendanceStatus;
    }

    public void setAttendanceStatus(AttendanceStatus attendanceStatus) {
        this.attendanceStatus = attendanceStatus;
    }
}

package com.anh.contactbook.model;

import com.anh.contactbook.entity.Heal;

import java.util.Date;
import java.util.List;

public class ClassChildrenHealth {
    String childId;
    String childName;
    Date birthDate;
    List<Heal> childHealth;
    int age;

    public String getChildId() {
        return childId;
    }

    public void setChildId(String childId) {
        this.childId = childId;
    }

    public String getChildName() {
        return childName;
    }

    public void setChildName(String childName) {
        this.childName = childName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public List<Heal> getChildHealth() {
        return childHealth;
    }

    public void setChildHealth(List<Heal> childHealth) {
        this.childHealth = childHealth;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}

package com.anh.contactbook.model;

public class NotificationQuantity {
    int application;
    int prescription;
    int systemlog;

    public int getApplication() {
        return application;
    }

    public void setApplication(int application) {
        this.application = application;
    }

    public int getPrescription() {
        return prescription;
    }

    public void setPrescription(int prescription) {
        this.prescription = prescription;
    }

    public int getSystemlog() {
        return systemlog;
    }

    public void setSystemlog(int systemlog) {
        this.systemlog = systemlog;
    }
}

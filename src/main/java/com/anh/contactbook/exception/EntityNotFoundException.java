package com.anh.contactbook.exception;

public class EntityNotFoundException extends RuntimeException {
    private String errorMessage;

    public EntityNotFoundException (String message) {
        super(message);
        this.errorMessage = message;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}

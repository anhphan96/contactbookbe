package com.anh.contactbook.exception;

public class EntityWasExistedException extends RuntimeException {

    private String errorMessage;

    public EntityWasExistedException (String message) {
        super(message);
        this.errorMessage = message;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}

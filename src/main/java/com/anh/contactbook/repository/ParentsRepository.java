package com.anh.contactbook.repository;
import com.anh.contactbook.entity.Parents;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface ParentsRepository extends MongoRepository<Parents, String> {
    List<Parents> findParentsByUserName(String userName);
    List<Parents> findAllByOrderByCreatedAtDesc();
}

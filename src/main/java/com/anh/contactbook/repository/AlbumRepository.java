package com.anh.contactbook.repository;

import com.anh.contactbook.entity.Album;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface AlbumRepository extends MongoRepository<Album, String> {
    Optional<Album> getAlbumById(String albumId);
    List<Album> getAlbumByClassIdOrderByCreatedAtDesc(String classId);
}

package com.anh.contactbook.repository;

import com.anh.contactbook.entity.Application;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Date;
import java.util.List;

public interface ApplicationRepository extends MongoRepository<Application, String> {
    List<Application> findApplicationByClassIdAndCreatedAt(String classId, Date date);
    List<Application> findApplicationByClassIdOrderByCreatedAtDesc(String classId);
    List<Application> findApplicationByClassIdAndChildIdOrderByCreatedAtDesc(String classId, String childId);
    int countApplicationByClassIdAndChildIdAndConfirmFalse(String classId, String childId);
    int countApplicationByClassIdAndConfirmFalse(String classId);
}

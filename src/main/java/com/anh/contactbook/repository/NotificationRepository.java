package com.anh.contactbook.repository;

import com.anh.contactbook.entity.Notification;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface NotificationRepository extends MongoRepository<Notification, String>{
    List<Notification> findNotificationByClassIdAndTeacherIdOrderByCreatedAtDesc(String classId, String teacherId);
    List<Notification> findNotificationByClassIdOrderByCreatedAtDesc(String classId);
}

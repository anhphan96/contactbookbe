package com.anh.contactbook.repository;

import com.anh.contactbook.entity.Teacher;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface TeacherRepository extends MongoRepository<Teacher, String> {
    List<Teacher> findAllByOrderByCreatedAtDesc();
    List<Teacher> findTeacherByUserName(String userName);
}

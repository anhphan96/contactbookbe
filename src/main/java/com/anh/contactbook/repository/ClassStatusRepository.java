package com.anh.contactbook.repository;

import com.anh.contactbook.entity.ClassStatus;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface ClassStatusRepository extends MongoRepository<ClassStatus, String> {
    List<ClassStatus> findClassStatusByClassIdAndCreatedAtBetween(String classId, Date begin, Date end);
    Optional<ClassStatus> findClassStatusByClassIdAndCreatedAt(String classId, Date createdAt);
}

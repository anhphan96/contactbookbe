package com.anh.contactbook.repository;

import com.anh.contactbook.entity.Attendance;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface AttendanceRepository extends MongoRepository<Attendance, String> {
    Optional<Attendance> findAttendanceByClassIdAndCreatedAt(String classId, Date createdDay);
    List<Attendance> findAttendanceByClassIdAndCreatedAtBetween(String classId, Date begin, Date end);
}

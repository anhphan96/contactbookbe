package com.anh.contactbook.repository;

import com.anh.contactbook.entity.Child;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface ChildRepository extends MongoRepository<Child, String> {
    List<Child> findChildByCurrentClassId(String classId);
    List<Child> findAllByOrderByCreatedAtDesc();
    List<Child> findChildByParentsId(String parentsId);
}

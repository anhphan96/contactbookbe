package com.anh.contactbook.repository;

import com.anh.contactbook.entity.ChildStatus;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface ChildStatusRepository extends MongoRepository<ChildStatus, String> {
    Optional<ChildStatus> getChildStatusByChildIdAndCreatedAt(String childId, Date createdAt);
    List<ChildStatus> getChildStatusByChildIdAndCreatedAtBetweenOrderByCreatedAtDesc(String childId, Date begin, Date end);
}

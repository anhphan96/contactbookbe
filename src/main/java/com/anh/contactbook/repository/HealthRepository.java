package com.anh.contactbook.repository;

import com.anh.contactbook.entity.Heal;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface HealthRepository extends MongoRepository<Heal, String> {
    List<Heal> getHealByChildIdOrderByAgeDesc(String childId);
    List<Heal> getHealByAge(int age);
}

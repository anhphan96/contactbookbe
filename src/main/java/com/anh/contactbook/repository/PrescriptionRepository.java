package com.anh.contactbook.repository;

import com.anh.contactbook.entity.Prescription;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface PrescriptionRepository extends MongoRepository<Prescription, String> {
    List<Prescription> getPrescriptionByChildIdOrderByConfirmDescCompleteDescCreatedAtDesc(String childId);
    List<Prescription> getPrescriptionByClassIdOrderByConfirmAscCompleteAscCreatedAtDesc(String classId);
    List<Prescription> getPrescriptionByChildIdOrderByCreatedAtDesc(String childId);
    int countPrescriptionByChildIdAndCompleteFalse(String childId);
    int countPrescriptionByClassIdAndCompleteFalse(String classId);
}

package com.anh.contactbook.repository;

import com.anh.contactbook.entity.SystemLog;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface SystemLogRepository extends MongoRepository<SystemLog, String> {
    List<SystemLog> findSystemLogByChildIdOrderByCreatedAtDesc(String childID);
    int countSystemLogByChildIdAndSeenFalse(String childId);
}

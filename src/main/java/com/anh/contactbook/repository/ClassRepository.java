package com.anh.contactbook.repository;

import com.anh.contactbook.entity.Class;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface ClassRepository extends MongoRepository<Class, String> {
    Optional<Class> findClassById(String id);
    List<Class> findClassByOrderByCreatedAtDesc();
}

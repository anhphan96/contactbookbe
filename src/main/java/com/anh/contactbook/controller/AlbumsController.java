package com.anh.contactbook.controller;

import com.anh.contactbook.entity.Album;
import com.anh.contactbook.service.AlbumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/albums")
@CrossOrigin
public class AlbumsController {
    @Autowired
    AlbumService albumService;

    @PostMapping()
    public ResponseEntity createAlbum(@RequestBody Album album) {
        Album createdAlbum = albumService.createAlbum(album);
        return new ResponseEntity(createdAlbum, HttpStatus.OK);
    }

    @GetMapping("/{albumId}")
    public ResponseEntity getAlbum(@PathVariable("albumId") String albumId) {
        Album returnAlbum = albumService.getAlbumByAlbumId(albumId);
        return new ResponseEntity(returnAlbum, HttpStatus.OK);
    }

    @GetMapping("/list/{classId}")
    public ResponseEntity getAlbumsByClassId(@PathVariable("classId") String classId) {
        List<Album> returnAlbums = albumService.getAlbumsByClassId(classId);
        return new ResponseEntity(returnAlbums, HttpStatus.OK);
    }

    @PostMapping("/photos")
    public ResponseEntity createPhotos(@RequestBody() Album album) {
        Album returnAlbum = albumService.createPhotos(album);
        return new ResponseEntity(returnAlbum, HttpStatus.OK);
    }

    @PutMapping()
    public ResponseEntity updateAlbum(@RequestBody() Album album) {
        Album returnAlbum = albumService.updateAlbum(album);
        return new ResponseEntity(returnAlbum, HttpStatus.OK);
    }

    @DeleteMapping("/{albumId}")
    public ResponseEntity deleteAlbum(@PathVariable("albumId") String albumId) {
        albumService.deleteAlbum(albumId);
        return new ResponseEntity(HttpStatus.OK);
    }
}

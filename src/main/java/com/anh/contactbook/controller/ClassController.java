package com.anh.contactbook.controller;

import com.anh.contactbook.entity.Class;
import com.anh.contactbook.service.ClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/class")
@CrossOrigin
@RestController
public class ClassController {
    private final ClassService classService;

    @Autowired
    public ClassController(ClassService classService) {
        this.classService = classService;
    }

    @GetMapping()
    public ResponseEntity getClasses() {
        return new ResponseEntity(this.classService.getClasses(), HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity saveTeacher(@RequestBody Class classs) {
        return new ResponseEntity(classService.saveClass(classs), HttpStatus.OK);
    }

    @PutMapping()
    public ResponseEntity closeClass(@RequestBody String classId) {
        classService.closeClass(classId);
        return new ResponseEntity(HttpStatus.OK);
    }

}

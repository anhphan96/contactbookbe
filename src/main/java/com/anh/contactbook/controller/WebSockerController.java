package com.anh.contactbook.controller;

import com.anh.contactbook.entity.SystemLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

@Controller
public class WebSockerController {
    private final SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    public WebSockerController(SimpMessagingTemplate simpMessagingTemplate) {
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

//    @MessageMapping("/{roomId}")
    public void onMessage(@DestinationVariable String roomId) {
        this.simpMessagingTemplate.convertAndSend("/privateRoom/" + roomId, "message");
    }

    public void confirmApplicationLog(String chanel, String chanelId, SystemLog systemLog) {
        this.simpMessagingTemplate.convertAndSend("/" + chanel + "/" + chanelId, systemLog);
    }

    public void createApplicationLog(String chanel, String classId, SystemLog systemLog) {
        this.simpMessagingTemplate.convertAndSend("/" + chanel + "/" + classId, systemLog);
    }
}

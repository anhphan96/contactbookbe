package com.anh.contactbook.controller;

import com.anh.contactbook.entity.Parents;
import com.anh.contactbook.service.ParentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/parents")
@CrossOrigin
@RestController
public class ParentsController {

    private final ParentsService parentsService;

    @Autowired
    public ParentsController(ParentsService parentsService) {
        this.parentsService = parentsService;
    }

    @GetMapping()
    public ResponseEntity getParents() {
        return new ResponseEntity(this.parentsService.getParents(), HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity saveTeacher(@RequestBody Parents parents) {
        return new ResponseEntity(parentsService.saveParents(parents), HttpStatus.OK);
    }

    @PostMapping("/account")
    public ResponseEntity saveAccount(@RequestBody Parents parents) {
        return new ResponseEntity(parentsService.saveAccount(parents), HttpStatus.OK);
    }
}

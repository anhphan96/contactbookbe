package com.anh.contactbook.controller;

import com.anh.contactbook.entity.Notification;
import com.anh.contactbook.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/notification")
@CrossOrigin
@RestController
public class NotificationController {
    private final NotificationService notificationService;

    @Autowired
    public NotificationController(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @GetMapping("/{classId}/{teacherId}")
    public ResponseEntity getNotifications (@PathVariable(value = "classId") String classId,
                                           @PathVariable(value = "teacherId") String teacherId) {
        return new ResponseEntity(notificationService.getNotificationsByClassIdAndTeacherId(classId, teacherId), HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity createNotification(@RequestBody Notification notification) {
        return new ResponseEntity(notificationService.saveNotification(notification),HttpStatus.OK);
    }

    @GetMapping("/{classId}")
    public ResponseEntity getNotificationsByClassId (@PathVariable(value = "classId") String classId) {
        return new ResponseEntity(notificationService.getNotificationsByClassId(classId), HttpStatus.OK);
    }
}

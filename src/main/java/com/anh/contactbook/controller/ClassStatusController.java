package com.anh.contactbook.controller;

import com.anh.contactbook.entity.ClassStatus;
import com.anh.contactbook.entity.Menu;
import com.anh.contactbook.entity.Schedule;
import com.anh.contactbook.service.ClassStatusService;
import com.anh.contactbook.untility.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RequestMapping("/class-status")
@CrossOrigin
@RestController
public class ClassStatusController {
    private final ClassStatusService classStatusService;

    @Autowired
    public ClassStatusController(ClassStatusService classStatusService) {
        this.classStatusService = classStatusService;
    }

    @GetMapping("/{id}")
    public ResponseEntity getClassStatus(@PathVariable(value = "id") String classId,
                                         @RequestParam String date) {
        Date createdAt = DateUtil.parseStringToDate(date);
        return new ResponseEntity(classStatusService.findClassStatusByClassIdAndCreatedAtBetween(classId, createdAt), HttpStatus.OK);
    }

//    @GetMapping("/{id}")
//    public ResponseEntity getClassStatusById(@PathVariable(value = "id") String classId,
//                                         @RequestParam String date) {
//        Date createdAt = DateUtil.parseStringToDate(date);
//        return new ResponseEntity(classStatusService.findClassStatusByClassIdAndCreatedAt(classId, createdAt), HttpStatus.OK);
//    }

    @PostMapping("/{classId}/{date}")
    public ResponseEntity createMenu(
            @PathVariable(value = "classId") String classId,
            @PathVariable(value = "date") String date,
            @RequestBody Menu menu) {
        Date createdAt = DateUtil.parseStringToDate(date);
        return new ResponseEntity(classStatusService.createMenu(menu, classId, createdAt),HttpStatus.OK);
    }

    @PutMapping("/{date}")
    public ResponseEntity updateMenu(
            @PathVariable(value = "date") String date,
            @RequestBody ClassStatus classStatus) {
        Date createdAt = DateUtil.parseStringToDate(date);
        return new ResponseEntity(classStatusService.updateMenu(classStatus, createdAt),HttpStatus.OK);
    }

    @GetMapping("/classStatus-week/{id}")
    public ResponseEntity getClassStatusInWeek (@PathVariable(value = "id") String classId,
                                                @RequestParam String date) {
        Date createdAt = DateUtil.parseStringToDate(date);
        return new ResponseEntity(classStatusService.findClassStatusByClassIdInTheWeek(classId, createdAt), HttpStatus.OK);
    }

    @PostMapping("schedule/{classId}/{date}")
    public ResponseEntity createSchedule(
            @PathVariable(value = "classId") String classId,
            @PathVariable(value = "date") String date,
            @RequestBody Schedule schedule) {
        Date createdAt = DateUtil.parseStringToDate(date);
        return new ResponseEntity(classStatusService.createSchedule(schedule, classId, createdAt),HttpStatus.OK);
    }

    @PutMapping("schedule/{date}")
    public ResponseEntity updateSchedule(
            @PathVariable(value = "date") String date,
            @RequestBody ClassStatus classStatus) {
        Date createdAt = DateUtil.parseStringToDate(date);
        return new ResponseEntity(classStatusService.updateSchedule(classStatus, createdAt),HttpStatus.OK);
    }
}

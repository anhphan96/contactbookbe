package com.anh.contactbook.controller;

import com.anh.contactbook.entity.Application;
import com.anh.contactbook.entity.Child;
import com.anh.contactbook.entity.Class;
import com.anh.contactbook.entity.ClassStatus;
import com.anh.contactbook.repository.ChildRepository;
import com.anh.contactbook.repository.ClassRepository;
import com.anh.contactbook.service.ApplicationService;
import com.anh.contactbook.service.ClassStatusService;
import com.anh.contactbook.untility.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/init")
public class InitController {

    private final ClassRepository classRepository;
    private final ChildRepository childRepository;
    private final ApplicationService applicationService;
    private final ClassStatusService classStatusService;

    @Autowired
    public InitController(ApplicationService applicationService, ChildRepository childRepository, ClassRepository classRepository, ClassStatusService classStatusService) {
        this.applicationService = applicationService;
        this.childRepository = childRepository;
        this.classRepository = classRepository;
        this.classStatusService = classStatusService;
    }

    @PostMapping("/class")
    public ResponseEntity createClass(@RequestBody Class createClass) {
        createClass.setCreatedAt(DateUtil.getDataBaseDate(createClass.getCreatedAt()));
        classRepository.save(createClass);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/child")
    public ResponseEntity createChild(@RequestBody Child child) {
        child.setCreatedAt(DateUtil.getDataBaseDate(child.getCreatedAt()));
        childRepository.save(child);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/application")
    public ResponseEntity createApplication(@RequestBody Application application) {
        this.applicationService.saveApplication(application);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/class-status")
    public ResponseEntity createClassStatus(@RequestBody ClassStatus classStatus) {
        classStatusService.saveOrCreateClassStatus(classStatus);
        return new ResponseEntity(HttpStatus.OK);
    }
}

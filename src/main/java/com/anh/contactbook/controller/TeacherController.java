package com.anh.contactbook.controller;

import com.anh.contactbook.entity.Teacher;
import com.anh.contactbook.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/teacher")
@CrossOrigin
@RestController
public class TeacherController {
    private final TeacherService teacherService;

    @Autowired
    public TeacherController(TeacherService teacherService) {
        this.teacherService = teacherService;
    }

    @GetMapping()
    public ResponseEntity getTeachers() {
        return new ResponseEntity(this.teacherService.getTeachers(), HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity saveTeacher(@RequestBody Teacher teacher) {
        return new ResponseEntity(teacherService.saveTeacher(teacher), HttpStatus.OK);
    }

    @PostMapping("/account")
    public ResponseEntity saveAccount(@RequestBody Teacher teacher) {
        return new ResponseEntity(teacherService.saveAccount(teacher), HttpStatus.OK);
    }

    @GetMapping("/{username}")
    public ResponseEntity getTeacherByUserName(@PathVariable(value = "username") String userName) {
        return new ResponseEntity(this.teacherService.getTeacherByAccount(userName), HttpStatus.OK);
    }
}

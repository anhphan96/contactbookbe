package com.anh.contactbook.controller;

import com.anh.contactbook.entity.Prescription;
import com.anh.contactbook.service.PrescriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/prescription")
@CrossOrigin
@RestController
public class PrescriptionController {

    private final PrescriptionService prescriptionService;

    @Autowired
    public PrescriptionController(PrescriptionService prescriptionService) {
        this.prescriptionService = prescriptionService;
    }

    @GetMapping("/{classId}")
    public ResponseEntity getPrescription (@PathVariable(value = "classId") String classId) {
        return new ResponseEntity(prescriptionService.getPrescriptionsByClassId(classId), HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity savePrescription(@RequestBody Prescription prescription) {
        return new ResponseEntity(prescriptionService.savePrescription(prescription), HttpStatus.OK);
    }

    @GetMapping("/confirm/{prescriptionId}")
    public ResponseEntity savePrescription(@PathVariable(value = "prescriptionId") String prescriptionId) {
        prescriptionService.confirmPrescription(prescriptionId);
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/individual/{childId}")
    public ResponseEntity getIndividualPrescription(@PathVariable(value = "childId") String childId) {
        return new ResponseEntity(prescriptionService.getIndividualPresciptions(childId), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity createIndividualPrescription(@RequestBody Prescription prescription) {
        return new ResponseEntity(prescriptionService.createIndividualPresciptions(prescription), HttpStatus.OK);
    }
}

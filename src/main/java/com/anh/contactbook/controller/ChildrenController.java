package com.anh.contactbook.controller;

import com.anh.contactbook.entity.Child;
import com.anh.contactbook.service.ChildService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/children")
@CrossOrigin
@RestController
public class ChildrenController {

    private final ChildService childService;

    @Autowired
    public ChildrenController(ChildService childService) {
        this.childService = childService;
    }

    @GetMapping("/{classId}")
    public ResponseEntity getChildrenByClassId(@PathVariable(value = "classId") String classId) {
        return new ResponseEntity(childService.getChildrenByClassId(classId), HttpStatus.OK);
    }

    @GetMapping()
    public ResponseEntity getChildren() {
        return new ResponseEntity(childService.getChildren(), HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity saveChild(@RequestBody Child child) {
        return new ResponseEntity(childService.saveChild(child), HttpStatus.OK);
    }

    @GetMapping("/info/{parentsId}")
    public ResponseEntity getChildrenByParents(@PathVariable(value = "parentsId") String parentsId) {
        return new ResponseEntity(childService.getChildrenByParents(parentsId), HttpStatus.OK);
    }
}

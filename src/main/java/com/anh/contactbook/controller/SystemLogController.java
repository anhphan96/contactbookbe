package com.anh.contactbook.controller;

import com.anh.contactbook.service.SystemLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/log")
@CrossOrigin
@RestController
public class SystemLogController {
    private final SystemLogService systemLogService;

    @Autowired
    public SystemLogController(SystemLogService systemLogService) {
        this.systemLogService = systemLogService;
    }

    @GetMapping("/{childId}")
    public ResponseEntity getPrescription (@PathVariable(value = "childId") String childId) {
        return new ResponseEntity(systemLogService.getSystemLogByChildId(childId), HttpStatus.OK);
    }
}

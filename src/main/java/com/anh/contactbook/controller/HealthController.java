package com.anh.contactbook.controller;

import com.anh.contactbook.entity.Heal;
import com.anh.contactbook.service.HealthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/health")
@CrossOrigin
@RestController
public class HealthController {

    private final HealthService healthService;

    @Autowired
    public HealthController(HealthService healthService) {
        this.healthService = healthService;
    }

    @GetMapping("/{childId}")
    public ResponseEntity getChildHealth(@PathVariable(value = "childId") String childId) {
        return new ResponseEntity(healthService.getHealthByChildId(childId), HttpStatus.OK);
    }

    @GetMapping("/by-class/{classId}")
    public ResponseEntity getChildHealthOfClass(@PathVariable(value = "classId") String classId) {
        return new ResponseEntity(healthService.getHealthByClassId(classId), HttpStatus.OK);
    }

    @PutMapping("/{childId}")
    public ResponseEntity updateChildHealth(@PathVariable(value = "childId") String childId,
                                            @RequestBody Heal heal) {
        return new ResponseEntity(healthService.saveChildHealth(childId, heal), HttpStatus.OK);
    }

    @DeleteMapping("/{healthId}")
    public ResponseEntity updateChildHealth(@PathVariable(value = "healthId") String healthId) {
        healthService.deleteHealthRecord(healthId);
        return new ResponseEntity(HttpStatus.OK);
    }
}

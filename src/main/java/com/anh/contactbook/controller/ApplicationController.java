package com.anh.contactbook.controller;

import com.anh.contactbook.entity.Application;
import com.anh.contactbook.entity.Attendance;
import com.anh.contactbook.service.ApplicationService;
import com.anh.contactbook.untility.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/application")
@CrossOrigin
public class ApplicationController {

    private final ApplicationService applicationService;

    @Autowired
    public ApplicationController(ApplicationService applicationService) {
        this.applicationService = applicationService;
    }

    @GetMapping("/{classId}")
    public ResponseEntity getAttendanceByCreatedDay(@PathVariable(value = "classId") String classId) {
        List<Application> applications = applicationService.getApplicationByClassIdAndCreatedAt(classId);
        return new ResponseEntity(applications, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity confirmAttendance(@RequestBody Application application) {
        applicationService.confirmApplication(application);
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/{classId}/{childId}")
    public ResponseEntity getIndividualApplications(@PathVariable(value = "classId") String classId,
                                                    @PathVariable(value = "childId") String childId) {
        List<Application> applications = applicationService.getApplicationByClassIdAndChildId(classId, childId);
        return new ResponseEntity(applications, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity createApplication(@RequestBody Application application) {
        Application returnApplication = this.applicationService.saveApplication(application);
        return new ResponseEntity(returnApplication, HttpStatus.OK);
    }
}

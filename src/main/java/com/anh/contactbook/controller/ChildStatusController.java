package com.anh.contactbook.controller;

import com.anh.contactbook.entity.ChildStatus;
import com.anh.contactbook.service.ChildStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/child-status")
@CrossOrigin
public class ChildStatusController {
    private final ChildStatusService childStatusService;

    @Autowired
    public ChildStatusController(ChildStatusService childStatusService) {
        this.childStatusService = childStatusService;
    }

    @GetMapping("/{classId}")
    public ResponseEntity getChildrenStatus (@PathVariable(value = "classId") String classId,
                                                @RequestParam String date) {
        return new ResponseEntity(childStatusService.getChildStatusByCurrentDate(classId, date), HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity updateChildrenStatus(@RequestBody List<ChildStatus> childStatuses) {
        childStatusService.save(childStatuses);
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/individual/{childId}")
    public ResponseEntity getIndividualChildrenStatus (@PathVariable(value = "childId") String childId,
                                                       @RequestParam(value = "date") String date) {
        return new ResponseEntity(childStatusService.getIndividualChildStatus(childId, date), HttpStatus.OK);
    }
}

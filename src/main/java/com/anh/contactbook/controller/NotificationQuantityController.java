package com.anh.contactbook.controller;

import com.anh.contactbook.service.NotificationQuantityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/count-noti")
@CrossOrigin
@RestController
public class NotificationQuantityController {

    final
    NotificationQuantityService notificationQuantityService;

    @Autowired
    public NotificationQuantityController(NotificationQuantityService notificationQuantityService) {
        this.notificationQuantityService = notificationQuantityService;
    }

    @GetMapping("/{classId}/{childId}")
    public ResponseEntity getNotiQuantityOfParent(@PathVariable(value = "classId") String classId,
                                                  @PathVariable(value = "childId") String childId) {
        return new ResponseEntity(notificationQuantityService.getNotificationQuantityOfParent(classId, childId), HttpStatus.OK);
    }

    @GetMapping("/{classId}")
    public ResponseEntity getNotiQuantityOfTeacher(@PathVariable(value = "classId") String classId) {
        return new ResponseEntity(notificationQuantityService.getNotificationQuantityOfTeacher(classId), HttpStatus.OK);
    }
}

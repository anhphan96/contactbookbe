package com.anh.contactbook.controller;

import com.anh.contactbook.entity.Attendance;
import com.anh.contactbook.model.IndividualAttendance;
import com.anh.contactbook.service.AttendanceService;
import com.anh.contactbook.untility.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/attendance")
@CrossOrigin
public class AttendanceController {

    private final AttendanceService attendanceService;

    @Autowired
    public AttendanceController(AttendanceService attendanceService) {
        this.attendanceService = attendanceService;
    }

    @GetMapping("/{classId}")
    public ResponseEntity getAttendanceByCreatedDay(@PathVariable(value = "classId") String classId,
                                                    @RequestParam(value = "date") String createdDay) {
        Date date = DateUtil.parseStringToDate(createdDay);
        Attendance attendance = attendanceService.getAttendanceByCreatedDay(classId, date);
        return new ResponseEntity(attendance, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity updateAttendance(@RequestBody Attendance attendance) {
        attendanceService.saveOrCreateAttendance(attendance);
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/{classId}/{childId}")
    public ResponseEntity getIndividualAttendance(@PathVariable(value = "classId") String classId,
                                                  @PathVariable(value = "childId") String childId,
                                                    @RequestParam(value = "date") String createdDay) {
        Date date = DateUtil.parseStringToDate(createdDay);
        List<IndividualAttendance> individualAttendances = attendanceService.getIndividualAttendance(classId, childId, date);
        return new ResponseEntity(individualAttendances, HttpStatus.OK);
    }
}
